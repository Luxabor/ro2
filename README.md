Operational Research 2 Project
===================

The purpose of this project is to illustrate various techniques used to approach
the Traveling Salesman Problem (TSP) and to compare the results obtained by
testing the aforementioned methods with respect to reference instances.


Project structure
-----------------

The project repository is structured as follows: in the root folder there is the CMakeList file,
which manages the build and linking steps, and the README.md file, which explains how the
repository is structured and how to execute the code.
The src folder contains the source code, the include folder groups the header files and
the data folder maintains the textual problem instances used to test the various
algorithms, ordered by type and dimension.
From the main.c file the function TSPopt it's called, and inside there are three TSPopt functions
corresponding to exact, heuristic and matheuristic approaches.


Paramenters description
-----------

For the exact algorithms the parameters are:
```
inputfile - the input instance on which the specified algorithms will be executed
verbose - level which represent the quantity of prints, 1 is minimum and 6 is max
solutionapproach - the algorithm used to solve the TSP problem
time_limit - the wall time limit for the algorithm used to solve the TSP problem
closestedges - number of closest nodes, used in heuristic_loop_method
optimumgap - precision of Cplex, below this value there is no difference between two numbers
printsolutiongap - boolean that triggers the informative callback
random_seed - random seed for the number generator
parallel_mode - parallel or opportunistic mode for Cplex (see Cplex documentation)
```

For the heuristic and matheuristic algorithms the parameters are:
```
inputfile - the input instance on which the specified algorithms will be executed
verbose - level which represent the quantity of prints, 1 is minimum and 6 is max
heuristic_approach - the algorithm that provides the warm start solution to the matheuristic algorithm
solutionapproach - the algorithm used to solve the TSP problem
time_limit - the wall time limit for the algorithm used to solve the TSP problem
perc_timelimit_ws - percentual of the time limit available for the search of a warm start (only apply to matheuristics)
random_seed - random seed for the number generator
parallel_mode - parallel or opportunistic mode for Cplex (see Cplex documentation)
```

Example of a command line for matheuristic method:
```
-inputfile ../data/ch130.tsp -verbose 1 -heuristic_approach multi -solutionapproach hard_fixing
-time_limit 20 -perc_timelimit_ws 0.2 -random_seed 38 -parallel_mode 1
```


Data format
-----------

We used the EUC-2D problems, each one defined by a set of nodes in R^2, and the distance
metric between them is the integer rounding of the euclidean distance.
Each problem is provided through a textual file divided in two sections: the specification
part presents various information on the file format and its content, while the
data part contains the node coordparallel_modeinates.

Example of an EUC-2D problem:
```
NAME: example10
TYPE: TSP
COMMENT: 10 locations in Rome
DIMENSION: 10
EDGE_WEIGHT_TYPE: EUC_2D
NODE_COORD_SECTION
1 565.0 575.0
2 25.0 185.0
3 345.0 750.0
4 945.0 685.0
5 845.0 655.0
6 880.0 660.0
7 25.0 230.0
8 525.0 1000.0
9 580.0 1175.0
10 650.0 1130.0
EOF
```


Further reading
---------------

Read the Report.pdf document, contained in the home folder of this repository, which contains the complete discussion of our implementation.