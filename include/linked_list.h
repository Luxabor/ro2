/****************************************
*   UNIVERSITY OF PADOVA                *
*                                       *
*   Operational Research 2 Project      *
*   Travelling Salesman Problem         *
*                                       *
*   Luca Borin                          *
*   Student ID: 1134473                 *
*   Email: borin.luca.93@gmail.com      *
*                                       *
*   Iacopo Mandatelli                   *
*   Student ID: 1151791                  *
*   Email: mandamondo@gmail.com         *
*                                       *
*****************************************/

#ifndef LINKED_LIST
#define LINKED_LIST

#include "data_types.h"
#include "utilities.h"

/**
 * Set the properties of a node in an array of nodes.
 * @param list Array of nodes.
 * @param self Position in the array.
 * @param prev Previous connection.
 * @param next Next connection.
 */
void set(node *list, int self, int prev, int next);

/**
 * Modifies two connections in a list of nodes.
 * @param list Array of nodes.
 * @param edge_1A
 * @param edge_2A
 * @param edge_1B
 * @param edge_2B
 */
void swap(node *list, int edge_1A, int edge_2A, int edge_1B, int edge_2B);

/**
 *
 * @param list
 * @param from
 * @param current
 * @return
 */
int select_tour_edge(node *list, int from, int current);

/**
 * Starting from an array of nodes, return a pointer to the destination node, considering the source and destination.
 * @param list Array of nodes.
 * @param source Source node id.
 * @param dest Destination node id.
 * @return Pointer to the destination node, NULL if an error occurred.
 */
node **select_destination_edge(node *list, int source, int dest);

/**
 * Starting from an array of nodes, return a pointer to the opposite node, considering the source and destination.
 * @param list Array of nodes.
 * @param source Source node id.
 * @param dest Destination node id.
 * @return Pointer to the opposite node, NULL if an error occurred.
 */
node **select_opposite_edge(node *list, int source, int dest);

#endif //LINKED_LIST
