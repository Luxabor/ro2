/****************************************
*   UNIVERSITY OF PADOVA                *
*                                       *
*   Operational Research 2 Project      *
*   Travelling Salesman Problem         *
*                                       *
*   Luca Borin                          *
*   Student ID: 1134473                 *
*   Email: borin.luca.93@gmail.com      *
*                                       *
*   Iacopo Mandatelli                   *
*   Student ID: 1151791                  *
*   Email: mandamondo@gmail.com         *
*                                       *
*****************************************/

#ifndef EXACT_TSP
#define EXACT_TSP

#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>

#include <ilcplex/cplex.h>
#include <pthread.h>
#include <unistd.h>
#include <concorde.h>
#include <semaphore.h>

#include "utilities.h"
#include "data_types.h"
#include "preprocessing.h"

#define EPSILON         0.1      // printing level  (=0 only incumbent, =1 little output, =2 good, =3 verbose, >=4 cplex log)


/**
 * Identifies the connected components for the current TSP solution.
 * @param inst Problem instance.
 */
void kruskal(instance *inst);

/**
 * Plot the TSP solution.
 * @param inst Problem instance.
 * @return Status.
 */
int plot(instance *inst);

/**
 * Build a TSP Cplex model.
 * @param inst Problem instance.
 * @param env Cplex env.
 * @param lp Cplex lp.
 */
void build_model(instance *inst, CPXENVptr env, CPXLPptr lp);

/**
 * Build a TSP Cplex model considering an heuristic limitation on the admissible edges.
 * @param inst Problem instance.
 * @param env Cplex env.
 * @param lp Cplex lp.
 */
void build_model_heuristic(instance *inst, CPXENVptr env, CPXLPptr lp);

/**
 * Compute the distance between the two provided points using the weight type stored in the problem instance.
 * @param inst Problem instance.
 * @param point1 First point.
 * @param point2 Second point.
 * @return Distance between point1 and point2.
 */
double dist(instance *inst, int point1, int point2);

/**
 * Map between the input point coordinates (user notation) and the Cplex 1d order.
 * @param i First point.
 * @param j Second point.
 * @param inst Problem instance.
 * @return Cplex coordinate.
 */
int xpos(int i, int j, instance *inst);

/**
 * Compute the integer euclidean distance between two points.
 * See the zib.de documentation for the details.
 * @param inst Problem instance.
 * @param point1 Point 1.
 * @param point2 Point 2.
 * @return Rounded integer distance.
 */
int euclideanDistance(instance *inst, int point1, int point2);

/**
 * Print the statistics about the solved instance, for analysis purpose.
 * @param inst Problem instance.
 * @param cpu_time Instant of start of the cpu clock.
 * @param wall_time Instant of start of the wall clock.
 */
void printStats(instance *inst, double cpu_time, double wall_time);

/**
 * Print the provided problem instance.
 * @param inst  Problem instance.
 */
void print_instance(instance *inst);

/**
 * Free the memory occupied by the provided problem instance.
 * @param inst Problem instance.
 */
void free_instance(instance *inst);

/**
 * Convert a .mst Cplex solution to a proper GnuPlot output file.
 * @param inst Problem instance.
 * @return Status.
 */
int tspfile_to_gnuplot(instance *inst);

/**
 * Convert a Cplex solution (vector structure) to a proper GnuPlot output file.
 * @param inst Problem instance.
 * @return Status.
 */
int tspvect_to_gnuplot(instance *inst);

/**
 * Obtain the current best solution to the provided cplex context.
 * @param env Cplex environment.
 * @param lp Cplex linear programming model.
 * @param inst Problem instance.
 * @return 0 success, non-zero error.
 */
int get_current_sol(CPXENVptr env, CPXLPptr lp, instance *inst);

/**
 * Implementatio of the loop method using subtours elimination for the TSP problem.
 * @param env Cplex environment.
 * @param lp Cplex linear programming model.
 * @param inst Problem instance.
 * @return 0 success, non-zero error.
 */
int loop_method(CPXENVptr env, CPXLPptr lp, instance *inst);

/**
 * Solve the problem instance using the loop method and subtours elimination.
 * @param env Cplex environment.
 * @param lp Cplex linear programming model.
 * @param inst Problem instance.
 * @return 0 success, non-zero error.
 */
int simple_loop_method(CPXENVptr env, CPXLPptr lp, instance *inst);

/**
 * Solve the problem instance using the loop method and cycles elimination.
 * @param env Cplex environment.
 * @param lp Cplex linear programming model.
 * @param inst Problem instance.
 * @return 0 success, non-zero error.
 */
int weak_loop_method(CPXENVptr env, CPXLPptr lp, instance *inst);

/**
 * Implementatio of the loop method using cycles elimination for the TSP problem.
 * @param env Cplex environment.
 * @param lp Cplex linear programming model.
 * @param inst Problem instance.
 * @return 0 success, non-zero error.
 */
int cycle_loop_method(CPXENVptr env, CPXLPptr lp, instance *inst);

/**
 * Solve the problem instance using an initial heuristic approach and loop method.
 * @param env Cplex environment.
 * @param lp Cplex linear programming model.
 * @param inst Problem instance.
 * @return 0 success, non-zero error.
 */
int heuristic_loop_method(CPXENVptr env, CPXLPptr lp, instance *inst);

/**
 * Solve the problem instance using the usercut callback method made available by Cplex.
 * @param env Cplex environment.
 * @param lp Cplex linear programming model.
 * @param inst Problem instance.
 * @return 0 success, non-zero error.
 */
int user_cut_method(CPXENVptr env, CPXLPptr lp, instance *inst);

/**
 * Solve the problem instance using the lazy callback method made available by Cplex.
 * @param env Cplex environment.
 * @param lp Cplex linear programming model.
 * @param inst Problem instance.
 * @param initialize Parameter used to control the initialization of Cplex.
 * @return 0 success, non-zero error.
 */
int lazy_constraint_method(CPXENVptr env, CPXLPptr lp, instance *inst, int initialize);

/**
 * Find a number in an array until the index size-1;
 * @param num The number to search.
 * @param array The array containing various numbers.
 * @param size Max index of the search.
 * @return position of the number in the array.
 */
int find(int num, int *array, int size);

/**
 * Using the pipe plot the current TSP solution.
 * @param gp Gnuplot associated pipe.
 * @param title Plot title.
 * @param inst Problem instance.
 */
void gnuprint(FILE *gp, char *title, instance *inst);

/**
 * Used to print information about the bounds of the problem inside the informative callback.
 * @param gp Gnuplot associated pipe.
 * @param inst Problem instance.
 * @param inte X-axis value, typically time or iteration.
 * @param bound Y-axis value, the best bound value.
 */
void gnuBoundPrint(FILE *gp, instance *inst, double inte, double bound);

/**
 * Find the top K closest vertexes to each vertex.
 * @param inst Problem instance.
 */
void heur_shortest_edges_bound(instance *inst);

/**
 * Replace the maximum length edge within the provided list.
 * @param input Edge to insert.
 * @param list List where to insert the edge.
 * @param size List size.
 * @return Maximum length of the edges contained in the list.
 */
double sorted_insert(edge input, edge *list, int size);

/**
 * Install the lazy callback.
 * @param env Cplex environment.
 * @param lp Cplex linear programming model.
 * @param inst Problem instance.
 */
void install_callback(CPXENVptr env, CPXLPptr lp, instance *inst);

/**
 * Lazy callback that provides the subtour elimination conditions.
 * @param env Cplex environment.
 * @param cbdata Callback input data.
 * @param wherefrom An integer value reporting where in the optimization this function was called.
 * @param cbhandle Problem instance.
 * @param useraction_p Callback result.
 * @return 0 success, non-zero error.
 */
static int CPXPUBLIC lazy_tsp_callback(CPXCENVptr env, void *cbdata, int wherefrom, void *cbhandle, int *useraction_p);

/**
 * Initialize a problem instance in order to assure thread safeness during the callback.
 * @param source Source problem instance.
 * @param dest Destination problem instance.
 */
void create_callback_instance(instance *source, instance *dest);

/**
 * Uninstall a lazy callback.
 * @param env Cplex environment.
 * @return 0 success, non-zero error.
 */
int uninstall_callback(CPXENVptr env, instance *inst);

/**
 * Initialize a problem instance.
 * @param env Cplex environment.
 * @param lp Cplex linear programming model.
 * @param inst Problem instance.
 */
void initialize_instance(CPXENVptr env, CPXLPptr lp, instance *inst);

/**
 * Informative callback, used inside the branch & bound algorithm to retrieve information about the current
 * visited node.
 * @param env Cplex environment.
 * @param cbdata Callback input data.
 * @param wherefrom An integer value reporting in which point of the optimization this function was called.
 * @param cbhandle Problem instance.
 * @param objval Cplex parameter, see documentation.
 * @param x Cplex parameter, see documentation.
 * @param isfeas_p Cplex parameter, see documentation.
 * @param useraction_p Callback result.
 * @return 0 success, non-zero error.
 */
static int CPXPUBLIC
info_tsp_callback(CPXCENVptr env, void *cbdata, int wherefrom, void *cbhandle, double objval, double *x, int *isfeas_p,
                  int *useraction_p);

/**
 * Usercut callback, called by Cplex, that provides the subtour elimination conditions also with non-integer solutions.
 * @param env Cplex environment.
 * @param cbdata Callback input data.
 * @param wherefrom An integer value reporting in which point of the optimization this function was called.
 * @param cbhandle Problem instance.
 * @param useraction_p Callback result.
 * @return 0 success, non-zero error.
 */
static int CPXPUBLIC cut_tsp_callback(CPXCENVptr env, void *cbdata, int wherefrom, void *cbhandle, int *useraction_p);

/**
 * Add flow cuts based on the Concorde callable library, this is used inside the usercut callback.
 * @param cutval Value of the cuts.
 * @param cutcount Number of cuts.
 * @param cut Array with the indexes of the variables affected by the cuts.
 * @param input_data Concorde formatted input.
 * @return 0 success, non-zero error.
 */
int add_flow_cut(double cutval, int cutcount, int *cut, void *input_data);

/**
 * Informs about the termination or not of the time-limit.
 * @param inst Problem instance.
 * @return 0 time-limit not expired, non-zero time-limit expired.
 */
int time_limit_expired(instance *inst);

/**
 * Simple implementation of the programming model for the TSP without subtour elimination constraints.
 * @param env Cplex environment.
 * @param lp Cplex linear programming model.
 * @param inst Problem instance.
 * @return 0 success, non-zero error.
 */
int no_subtour(CPXENVptr env, CPXLPptr lp, instance *inst);


#endif //EXACT_TSP
