/****************************************
*   UNIVERSITY OF PADOVA                *
*                                       *
*   Operational Research 2 Project      *
*   Travelling Salesman Problem         *
*                                       *
*   Luca Borin                          *
*   Student ID: 1134473                 *
*   Email: borin.luca.93@gmail.com      *
*                                       *
*   Iacopo Mandatelli                   *
*   Student ID: 1151791                  *
*   Email: mandamondo@gmail.com         *
*                                       *
*****************************************/

#ifndef UTILITIES
#define UTILITIES

#include <stdio.h>
#include <time.h>
#include <stdlib.h>
#include <string.h>

/**
 * Print an error message and exit from the program.
 * @param err Error message.
 */
void print_error(const char *err);

/**
 * Start the cpu clock.
 * See GNU C Library for more details about CPU-Time.
 * @return The starting instant fo the cpu clock.
 */
clock_t clockStart();

/**
 * Stop the cpu clock.
 * @param cpu_start The starting instant fo the cpu clock.
 * @return The cpu time elapsed from @cpu_start .
 */
double clockEnd(clock_t cpu_start);

/**
 * Start the wall clock.
 * See GNU C Library for more details about Wall-Time.
 * @return The starting instant for the wall clock.
 */
struct timespec wallStart();

/**
 * Stop the wall clock.
 * @param wall_start The starting instant fo the wall clock.
 * @return The wall time elapsed from @wall_start .
 */
double wallEnd(struct timespec wall_start);

/**
 * Prints out the meaning of the CPLEX solution code.
 * @param code CPLEX solution code (see http://www.rpi.edu/dept/math/math-programming/cplex66/sun4x_56/doc/refman/html/appendixB.html#151095)
 * @param explanation Explanation for the corresponding provided code.
 */
void solution_code(int code, char *explanation);

/**
 * Compute the factorial with a certain base.
 * @param num Factorial base.
 * @return Factorial.
 */
int factorial(int num);

/**
 * Return the combinations without repetition.
 * @param n
 * @param k
 * @return (n k)
 */
int comb(int n, int k);

#endif //UTILITIES
