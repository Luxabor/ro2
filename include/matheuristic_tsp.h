/****************************************
*   UNIVERSITY OF PADOVA                *
*                                       *
*   Operational Research 2 Project      *
*   Travelling Salesman Problem         *
*                                       *
*   Luca Borin                          *
*   Student ID: 1134473                 *
*   Email: borin.luca.93@gmail.com      *
*                                       *
*   Iacopo Mandatelli                   *
*   Student ID: 1151791                  *
*   Email: mandamondo@gmail.com         *
*                                       *
*****************************************/

#ifndef MATHEURISTIC_TSP
#define MATHEURISTIC_TSP

#include "data_types.h"
#include "heuristic_tsp.h"
#include "exact_tsp.h"

/**
 * Main function that solves the TSP problem using the Local Branching technique.
 * It needs an adequate preprocessing.
 * @param env Cplex environment.
 * @param lp Cplex linear programming model.
 * @param inst_tour Problem instance, see data_types.h.
 * @param inst Problem instance.
 * @return 0 success, non-zero error.
 */
int local_branching(CPXENVptr env, CPXLPptr lp, tour *inst_tour, instance *inst);

/**
 * Utility function that order the solution produced by Cplex (stored in tour) into sorted_tour
 * @param tour Problem instance, see data_types.h.
 * @param sorted_tour Ordered tour of nodes.
 * @param nnodes Number of nodes.
 */
void order_solution(point *tour, int *sorted_tour, int nnodes);

/**
 * Hard Fixing algorithm.
 * @param env Cplex environment.
 * @param lp Cplex linear programming model.
 * @param tour_inst Tour instance.
 * @param inst Problem instance.
 * @return  0 success, non-zero error.
 */
int hard_fixing(CPXENVptr env, CPXLPptr lp, tour *tour_inst, instance *inst);

/**
 * Set the lower and upper bound of all the problem variables. Useful to remove previous constraints.
 * @param env Cplex environment.
 * @param lp Cplex linear programming model.
 * @param inst Problem instance.
 * @param lb Lower bound.
 * @param ub Upper bound.
 */
void set_lb_and_ub(CPXENVptr env, CPXLPptr lp, instance *inst, double lb, double ub);

/**
 * Fix certain edges to zero in order to avoid useless effort of the MIP solver: forbid obvious separated connected components.
 * @param env Cplex environment.
 * @param lp Cplex linear programming model.
 * @param inst Problem instance.
 * @param sorted_tour Sorted tour.
 * @param fixed_edge Edges fixed randomly during the Hard Fixing first loop.
 * @param nnodes Number of nodes of the provided problem.
 */
void polish(CPXENVptr env, CPXLPptr lp, instance *inst, int *sorted_tour, int *fixed_edge, int nnodes);

/**
 * Transformation function between datatypes.
 * @param best_sol Array containing the values of the variables of the model.
 * @param inst Problem instance, see data_types.h.
 * @param tour_inst Problem instance, see data_types.h.
 * @param nnodes Number of nodes.
 */
void instance_to_tour(double *best_sol, instance *inst, tour *tour_inst, int nnodes);


#endif //MATHEURISTIC_TSP
