/****************************************
*   UNIVERSITY OF PADOVA                *
*                                       *
*   Operational Research 2 Project      *
*   Travelling Salesman Problem         *
*                                       *
*   Luca Borin                          *
*   Student ID: 1134473                 *
*   Email: borin.luca.93@gmail.com      *
*                                       *
*   Iacopo Mandatelli                   *
*   Student ID: 1151791                  *
*   Email: mandamondo@gmail.com         *
*                                       *
*****************************************/

#ifndef SOLVER
#define SOLVER

#include "exact_tsp.h"
#include "heuristic_tsp.h"
#include "matheuristic_tsp.h"

/**
 * Initialize the Cplex model and set various parameters of Cplex, such as
 * precision, time-limit and the verbose level
 * @param env Cplex environment.
 * @param lp Cplex linear programming model.
 * @param inst Problem instance.
 * @return non-zero an error occurred
 */
int initialize_Cplex(CPXENVptr *env, CPXLPptr *lp, instance *inst);

/**
 * Solves the TSP problem using heuristic techniques
 * @param argc Number of input parameters.
 * @param argv Command line input parameters.
 * @return 0 success, non-zero error.
 */
int TSPopt_heuristic(int argc, char **argv);

/**
 * Solves the TSP problem using matheuristic techniques
 * @param argc Number of input parameters.
 * @param argv Command line input parameters.
 * @return 0 success, non-zero error.
 */
int TSPopt_matheuristic(int argc, char **argv);

/**
 * Solves the TSP problem using exact techniques
 * @param argc Number of input parameters.
 * @param argv Command line input parameters.
 * @return 0 success, non-zero error.
 */
int TSPopt_exact(int argc, char **argv);

/**
 * Entry point.
 * @param argc Number of input parameters.
 * @param argv Command line input parameters.
 * @return 0 success, non-zero error.
 */
int TSPopt(int argc, char **argv);


#endif //SOLVER
