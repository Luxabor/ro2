/****************************************
*   UNIVERSITY OF PADOVA                *
*                                       *
*   Operational Research 2 Project      *
*   Travelling Salesman Problem         *
*                                       *
*   Luca Borin                          *
*   Student ID: 1134473                 *
*   Email: borin.luca.93@gmail.com      *
*                                       *
*   Iacopo Mandatelli                   *
*   Student ID: 1151791                  *
*   Email: mandamondo@gmail.com         *
*                                       *
*****************************************/

#ifndef DATA_TYPES
#define DATA_TYPES

#include <ilcplex/cplex.h>

/**
 * Structure used to memorized a point in the euclidean space.
 */
typedef struct
{
    double x;      //x coordinate of the point
    double y;      //y coordinate of the point
} point;

typedef struct
{
    int v1;
    int v2;
    double length;
} edge;

typedef struct
{
    int v1;
    int v2;
} unweighted_edge;

typedef struct
{
    //Number of points
    int nnodes;

    //Collection of points
    point *coord;

    //Kind of weight used
    char weight_type[100];
} problem;

/**
 * Structure used to memorize the information about a TSP problem
 */
typedef struct
{
    /*------------------------------------------------------------
     * PROBLEM DATA
    --------------------------------------------------------------*/
    //Problem info
    problem *prob;

    //Heuristic shortest edges
    edge **heur_shortest_edges;

    //Number of edges used in the heuristic loop approach
    int heur_num_shortest_edges;

    //Gap from the optimal solution
    double opt_gap;

    /*------------------------------------------------------------
     * SOLUTION
    --------------------------------------------------------------*/

    //Best path available
    double *best_sol;

    //Best lower bound available
    double best_lb;

    //Best solution available
    double best_val;

    //Number of model variables
    int ncols;

    //Id of the connected component for each vertex
    int *connected_vertex;

    int loop_iterations;

    /*------------------------------------------------------------
     * PARAMETERS
    --------------------------------------------------------------*/

    //Number of threads that can be used during the solution
    int n_threads;

    //Cplex random seed
    int random_seed;

    int parallel_mode;

    //Accumulators of the time inside each callback
    double *time_lazy;
    double *time_usercut;

    //Instant of start of the solving
    struct timespec wall_start;

    clock_t cpu_start;

    //print level
    int verbose;

    // Overall time limit [s]
    double timelimit;

    // Percentage of the time limit that should be dedicated to the heuristic solver to find a warm start for the
    // matheuristic approach
    double perc_timelimit_ws;

    //Indicates the problem solution approach
    char solutionapproach[100];

    char heuristic_approach[100];

    //Indicate the heuristic that should be used to provide a warm start for the matheuristic approach
    char heuristic_warm_start[100];

    //Indicates whether to print or not the gap between the solution best bound and the best integer
    //(only with lazy constraint method)
    char printsolutiongap[10];

    //Input file name
    char inputfile[100];

    //Name of the output plot file used by Gnuplot
    char plotfile[100];

    //Name of the solution file produced by Cplex
    char solutionfile[100];

    //Name of the Gnuplot script for the solution plot
    char gnuplotfile[100];

    //Pipe to graph the TSP solution using Gnuplot
    FILE *gnuplotPipe;

    //Pipe to graph the gap reduction between the best integer and the best bound
    FILE *gnuplotBoundPipe;
} instance;

typedef struct
{
    instance *inst;
    CPXCENVptr env;
    void *cbdata;
    int wherefrom;
    int *useraction_p;

} input_concorde;

typedef struct node
{
    int val;
    struct node *next;
    struct node *prev;
} node;

typedef struct
{
    problem *instance_problem;

    node *tour_nodes;

    double cost;

    int start_node;

    double probability;

    int random_seed;

    double timelimit;

    //Instant of start of the solving
    struct timespec wall_start;

    //Verbosity level
    int verbose;

    //Pipe to graph the TSP solution using Gnuplot
    FILE *gnuplotPipe;

    //Input file name
    char inputfile[100];

    //Indicates the problem solution approach
    char solutionapproach[100];

    char heuristic_approach[100];

} tour;

typedef struct
{
    problem *instance_problem;

    int *nodes;

    double temperature;

    long long int iterations;

    double cost;

    double timelimit;

    int random_seed;

    struct timespec wall_start;

    //Input file name
    char inputfile[100];

    //Pipe to graph the TSP solution using Gnuplot
    FILE *gnuplotPipe;

    int verbose;

    //Indicates the problem solution approach
    char solutionapproach[100];

    char heuristic_approach[100];

} simulated_instance;

#endif //DATA_TYPES
