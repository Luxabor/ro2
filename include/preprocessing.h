/****************************************
*   UNIVERSITY OF PADOVA                *
*                                       *
*   Operational Research 2 Project      *
*   Travelling Salesman Problem         *
*                                       *
*   Luca Borin                          *
*   Student ID: 1134473                 *
*   Email: borin.luca.93@gmail.com      *
*                                       *
*   Iacopo Mandatelli                   *
*   Student ID: 1151791                  *
*   Email: mandamondo@gmail.com         *
*                                       *
*****************************************/

#ifndef PREPROCESSING
#define PREPROCESSING

#include "data_types.h"
#include "utilities.h"
#include "math.h"

/**
 * Read the input parameters provided through the command line execution.
 * @param argc Number of parameters.
 * @param argv Parameters.
 * @param inst Problem instance.
 */
void read_command_line(int argc, char **argv, instance *inst);

/**
 * Read a TSP representation under the .tsp standard format.
 * @param input_file Input file.
 * @param prob Problem instance.
 */
void read_problem(char *input_file, problem *prob);

/**
 * Read the input parameters specific to the heuristic algorithms provided through the command line execution.
 * @param argc Number of parameters.
 * @param argv Parameters.
 * @param inst_tour
 * @return
 */
int read_command_line_heuristic(int argc, char **argv, tour *inst_tour);

/**
 * Read the input parameters specific to simulated annealing provided through the command line execution.
 * @param argc Number of parameters.
 * @param argv Parameters.
 * @param inst
 * @return
 */
int read_command_line_simulated_annealing(int argc, char **argv, simulated_instance *inst);

/**
 * Read the input parameters specific to the matheuristic algorithms provided through the command line execution.
 * @param argc Number of parameters.
 * @param argv Parameters.
 * @param sim_inst Problem instance, see data_types.h.
 * @param tour_inst Problem instance, see data_types.h.
 * @param inst Problem instance, see data_types.h.
 * @return
 */
int read_command_line_matheuristic(int argc, char **argv, simulated_instance *sim_inst, tour *tour_inst,
                                   instance *inst);

/**
 * Function that allocate the necessary memory for the instance datatype.
 * @return Pointer to the created instance.
 */
instance *create_instance();

/**
 * Function that allocate the necessary memory for the tour datatype.
 * @return Pointer to the created tour.
 */
tour *create_tour();

/**
 * Function that allocate the necessary memory for the simulated_instance datatype.
 * @return Pointer to the created simulated instance.
 */
simulated_instance *create_sim_inst();


#endif //PREPROCESSING