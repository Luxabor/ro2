/****************************************
*   UNIVERSITY OF PADOVA                *
*                                       *
*   Operational Research 2 Project      *
*   Travelling Salesman Problem         *
*                                       *
*   Luca Borin                          *
*   Student ID: 1134473                 *
*   Email: borin.luca.93@gmail.com      *
*                                       *
*   Iacopo Mandatelli                   *
*   Student ID: 1151791                  *
*   Email: mandamondo@gmail.com         *
*                                       *
*****************************************/

#ifndef HEURISTIC_TSP
#define HEURISTIC_TSP

#include <math.h>
#include "data_types.h"
#include "utilities.h"
#include <string.h>
#include <stdio.h>
#include <stdbool.h>
#include "linked_list.h"
#include "preprocessing.h"

/***********************************************
 * NEAREST NEIGHBOR
 **********************************************/

/**
 * Produce a randomized nearest neighbor tour.
 * @param inst_tour Output tour.
 * @param start_node Starting node.
 * @param seed Random seed.
 * @return 0 if everything ok, -1 if an error occurred.
 */
int nearest_neighbor(tour *inst_tour, int start_node, unsigned int *seed);

/**
 * Utility method for the nearest neighbor function, finds the nearest vertex to the given one.
 * @param inst_tour Input tour.
 * @param origin_vertex Origin vertex.
 * @param start_node Start node.
 * @param probability Probability to accept the closest vertex, otherwise the second or third one.
 * @param seed Random seed.
 * @return
 */
int nearest_vertex(tour *inst_tour, int origin_vertex, int start_node, double probability, unsigned int *seed);

/***********************************************
 * MULTISTART
 **********************************************/

/**
 * Parallelized multistart.
 * @param inst_tour  Input/Output tour.
 * @return 0 if everything ok, -1 if an error occurred.
 */
int solve_heuristic_tsp_multi(tour *best_tour);

/**
 * Multistart.
 * @param inst_tour  Input/Output tour.
 * @return 0 if everything ok, -1 if an error occurred.
 */
int solve_heuristic_tsp_mono(tour *best_tour);

/***********************************************
 * 2-OPT FUNCTIONS
 **********************************************/

/**
 * Perform a 2-swap move.
 * @param inst_tour Input/Output tour.
 * @param type 0 for the "first-swap" approach, 1 for the "best-swap" approach.
 * @return 0 if everything ok, -1 if an error occurred.
 */
int opt_2(tour *inst_tour, int type);

/**
 * Perform the first ameliorative 2-swap move found.
 * @param inst_tour Input/Output tour.
 * @return 0 if everything ok, -1 if an error occurred.
 */
int opt_2_first_swap(tour *inst_tour);

/**
 * Perform the best 2-swap move found in terms of cost reduction.
 * @param inst_tour Input/Output tour.
 * @return 0 if everything ok, -1 if an error occurred.
 */
int opt_2_best_swap(tour *inst_tour);

/***********************************************
 * VNS
 **********************************************/

/**
 * Variable Neighbor Search.
 * @param best_tour Best tour found.
 * @return 0 if everything ok, -1 if an error occurred.
 */
int VNS(tour *best_tour);

/**
 * Starting from a tour stored through linked lists, create an array version travelling it.
 * @param inst_tour Input tour.
 * @param sorted_tour_next Output "sorted" tour.
 */
void create_sorted_tour(tour *inst_tour, int *sorted_tour_next);

/**
 * Utily function, used to remap information between datatypes.
 * @param inst_tour Input tour.
 * @param sorted_tour Output "sorted" tour.
 */
void create_sorted_tour_annealing(tour *inst_tour, int *sorted_tour);

/**
 * Utily function, used to order the array of nodes.
 * @param sorted_tour Output "sorted" tour.
 * @param start_node Start node of the tour.
 * @param nnodes Number of nodes.
 */
void create_ordered_tour(int *sorted_tour, int start_node, int nnodes);

/**
 * Extract 3 random edges from the tour (array version).
 * @param sorted_edges Input tour (array version).
 * @param nnodes Number of nodes.
 * @param edges Output edges.
 */
void opt_3_find_valid_edges_sorted(int *sorted_edges, int nnodes, int *edges);

/**
 * Perform a valid 3 opt move (array version).
 * @param inst_tour Input/Output tour
 */
void opt_3_perform_move_sorted(tour *inst_tour);

/**
 * Extract 3 random edges from the tour (linked list version).
 * @param inst_tour Input tour.
 * @param edges Output edges.
 */
void opt_3_find_valid_edges_not_sorted(tour *inst_tour, int *edges);

/**
 * Perform a valid 3 opt move (linked list version).
 * @param inst_tour Input/Output tour
 */
void opt_3_perform_move_not_sorted(tour *inst_tour);

/**
 * Count the amount of vertexes in a connected component of the provided tour.
 * @param inst_tour Input
 * @return Number of vertexes.
 */
int opt_3_count_tour_edges_not_sorted(tour *inst_tour);

/**
 * Execute a 3-opt move, leading to the edges a1-b2, a2-c1, b1-c2.
 * @param inst_tour Input tour.
 * @param a1 Edge A first vertex.
 * @param a2 Edge A second vertex.
 * @param b1 Edge B first vertex.
 * @param b2 Edge B second vertex.
 * @param c1 Edge C first vertex.
 * @param c2 Edge C second vertex.
 * @param random_edges Input edges A,B,C.
 * @return New combination cost - Old combination cost.
 */
double swap_opt_3(tour *inst_tour, int a1, int a2, int b1, int b2, int c1, int c2, int *random_edges);

/**
 * Execute a reverse 3-opt move, leading to the edges a1-a2, b1-b2, c1-c2, previously modified by swap_opt_3.
 */
double reverse_swap_opt_3(tour *inst_tour, int a1, int a2, int b1, int b2, int c1, int c2, int *random_edges);

/**
 * Check if the 3 provided edges are valid with respect to the 3-opt move (not adjacent).
 * @param edges Input edges.
 * @return 0 if valid, -1 if not valid.
 */
int opt_3_check_edges(int *edges);

/**
 * Sort the edges in order to make them respect the tour order.
 * @param sorted_tour Input tour (array version).
 * @param edges Input/Output edges.
 */
void sort_tour_edges(int *sorted_tour, int *edges);

/***********************************************
 * SIMULATED ANNEALING
 **********************************************/

/**
 * Free the memory occupied by the instance.
 * @param inst Problem instance.
 */
void free_simulated_instance(simulated_instance *inst);

/**
 * Print function used inside the simulated to analyze useful stats.
 * @param inst Problem instance.
 * @param title Title of the plot.
 */
void simulated_annealing_gnuprint(simulated_instance *inst, char *title);

/**
 * Main function that solves the TSP problem using the simulated annealing technique.
 * @param inst Problem instance.
 * @return 0 success, non-zero error.
 */
int solve_simulated_annealing(simulated_instance *inst);

/**
 * Wrapper function used inside the matheuristic methods, calls the main function.
 * @param sim_inst Problem instance, see data_types.h.
 * @param inst_tour Problem instance, see data_types.h.
 * @return
 */
int solve_simulated_annealing_tour(simulated_instance *sim_inst, tour *inst_tour);

/**
 * Swap the elements with idexe i and j of the array.
 * @param array Input array.
 * @param i First index.
 * @param j Second index.
 */
void swap_simulated_annealing(int *array, int i, int j);

/**
 * Function that generate an initial random tour, doing |nnodes| random swaps/permutations, and calculate its cost.
 * @param inst Problem instance.
 * @return Cost of the generate tour.
 */
double generate_initial_tour(simulated_instance *inst);

/**
 * Generate a new candidate tour, reversing a random portion of the tour, based on the old tour.
 * @param inst Problem instance.
 * @param new_tour Candidate tour.
 * @return Cost of the generate candidate tour.
 */
double generate_new_tour_1(simulated_instance *inst, int *new_tour);

/**
 * Generate a new candidate tour, doing up to |nnodes| random swaps/permutations, based on the old tour.
 * @param inst Problem instance.
 * @param new_tour Candidate tour.
 * @return Cost of the generate candidate tour.
 */
double generate_new_tour_2(simulated_instance *inst, int *new_tour);

/**
 * Acceptance probability for the candidate tour based on its cost, the cost of the previous tour and the
 * temperature of the system.
 * @param old_cost Cost of the old tour.
 * @param new_cost Cost of the candidate tour.
 * @param temperature Temperature of the system.
 * @return 1 if the candidate tour has been accepted, 0 otherwise.
 */
int probability(double old_cost, double new_cost, double temperature);

/***********************************************
 * UTILITY
 **********************************************/

/**
 * Compute the integer euclidean distance between two points.
 * See the zib.de documentation for the details.
 * @param inst Problem instance.
 * @param point1 Point 1.
 * @param point2 Point 2.
 * @return Rounded integer distance.
 */
int euclidean_distance(problem *inst, int point1, int point2);

/**
 * Return the distance between two points.
 * @param inst Input tour.
 * @param point1 First point.
 * @param point2 Second point.
 * @return Distance between point1 and point2.
 */
double distance(tour *inst, int point1, int point2);

/**
 * Free the memory occupied by a tour.
 * @param inst_tour Input tour.
 */
void free_tour(tour *inst_tour);

/**
 * Reset the tour nodes.
 * @param source Input tour.
 */
void clear_tour(tour *source);

/**
 * Print a tour.
 * @param inst_tour Input tour.
 */
void print_tour(tour *inst_tour);

/**
 * Print a tour (array version).
 * @param sorted_tour Input tour (array version).
 * @param start_node Starting node.
 * @param nnodes Number of nodes.
 */
void print_tour_sorted(int *sorted_tour, int start_node, int nnodes);

/**
 * Print a tour using Gnuplot.
 * @param inst_tour Input tour.
 * @param title Chart title.
 */
void heuristic_gnuprint(tour *inst_tour, char *title);

/**
 * Copy an entire tour instance.
 * @param src_tour Input tour.
 * @param dest_tour Output tour.
 */
void copy_instance(tour *src_tour, tour *dest_tour);

/**
 * Copy only the tour of a certain instance.
 * @param source Input instance.
 * @param destination Output instance.
 */
void copy_tour(tour *source, tour *destination);

/**
 * Find a number in an array until the index size-1;
 * @param num The number to search.
 * @param array The array containing various numbers.
 * @param size Max index of the search.
 * @return position of the number in the array.
 */
int find_int(int num, int *array, int size);

/**
 * Count the number of occurrences of a number inside an array.
 * @param num Target number.
 * @param array Input array.
 * @param size Size of the input array.
 * @return Number of occurrences.
 */
int count_occurrences(int num, int *array, int size);

/**
 * Print the statistics about the solved instance, for analysis purpose.
 * @param tour_inst Problem instance, see data_types.h.
 * @param sim_inst Problem instance, see data_types.h.
 */
void print_stats(tour *tour_inst, simulated_instance *sim_inst);


#endif //HEURISTIC_TSP
