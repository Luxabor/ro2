mv $(find . -type f -exec grep -l 'EUC_2D' {} \;) ${PWD}/EUC_2D/
mv $(find . -type f -exec grep -l 'GEO' {} \;) ${PWD}/GEO/
mv $(find . -type f -exec grep -l 'ATT' {} \;) ${PWD}/ATT/
mv $(find . -type f -exec grep -l 'EXPLICIT' {} \;) ${PWD}/EXPLICIT/
mv $(find . -type f -exec grep -l 'CEIL_2D' {} \;) ${PWD}/CEIL_2D/
