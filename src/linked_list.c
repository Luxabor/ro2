/****************************************
*   UNIVERSITY OF PADOVA                *
*                                       *
*   Operational Research 2 Project      *
*   Travelling Salesman Problem         *
*                                       *
*   Luca Borin                          *
*   Student ID: 1134473                 *
*   Email: borin.luca.93@gmail.com      *
*                                       *
*   Iacopo Mandatelli                   *
*   Student ID: 1151791                  *
*   Email: mandamondo@gmail.com         *
*                                       *
*****************************************/

#include "linked_list.h"

void set(node *list, int self, int prev, int next)
{
    list[self].prev = &(list[prev]);
    list[self].next = &(list[next]);
}

void swap(node *list, int edge_1A, int edge_2A, int edge_1B, int edge_2B)
{
    node **correct_pointer;

    correct_pointer = select_destination_edge(list, edge_1A, edge_2A);
    *correct_pointer = &(list[edge_1B]);

    correct_pointer = select_destination_edge(list, edge_2A, edge_1A);
    *correct_pointer = &(list[edge_2B]);

    correct_pointer = select_destination_edge(list, edge_1B, edge_2B);
    *correct_pointer = &(list[edge_1A]);

    correct_pointer = select_destination_edge(list, edge_2B, edge_1B);
    *correct_pointer = &(list[edge_2A]);
}


node **select_destination_edge(node *list, int source, int dest)
{
    if (list[source].next->val == dest)
        return &(list[source].next);
    else if (list[source].prev->val == dest)
        return &(list[source].prev);
    else
        return NULL;
}

node **select_opposite_edge(node *list, int source, int dest)
{
    if (list[source].next->val == dest)
        return &(list[source].prev);
    else if (list[source].prev->val == dest)
        return &(list[source].next);
    else
        return NULL;
}

int select_tour_edge(node *list, int from, int current)
{
    if (list[current].next->val == from)
        return list[current].prev->val;
    else
        return list[current].next->val;
}