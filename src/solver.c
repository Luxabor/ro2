/****************************************
*   UNIVERSITY OF PADOVA                *
*                                       *
*   Operational Research 2 Project      *
*   Travelling Salesman Problem         *
*                                       *
*   Luca Borin                          *
*   Student ID: 1134473                 *
*   Email: borin.luca.93@gmail.com      *
*                                       *
*   Iacopo Mandatelli                   *
*   Student ID: 1151791                  *
*   Email: mandamondo@gmail.com         *
*                                       *
*****************************************/

#include "solver.h"

int initialize_Cplex(CPXENVptr *env, CPXLPptr *lp, instance *inst)
{
    //Create a Cplex model
    int status;

    *env = CPXopenCPLEX(&status);
    if (status) print_error("Error occurred creating Cplex environment.\n");

    *lp = CPXcreateprob(*env, &status, "TSP");
    if (status) print_error("Error occurred creating Cplex problem.\n");

    //Precision
    CPXsetdblparam(*env, CPX_PARAM_EPINT, 0.0);
    CPXsetdblparam(*env, CPX_PARAM_EPGAP, 1e-9);
    CPXsetdblparam(*env, CPX_PARAM_EPRHS, 1e-9);

    //Set time limit
    CPXsetdblparam(*env, CPX_PARAM_TILIM, inst->timelimit);

    // random seed and parallel mode
    if (inst->random_seed != 0)
        CPXsetintparam(*env, CPX_PARAM_RANDOMSEED, inst->random_seed);

    if (inst->parallel_mode != -1 && inst->parallel_mode != 1)
    {
        print_error("Set the proper value for the parallel mode: Opportunistic = -1, Deterministic = 1.\n");
    }

    CPXsetintparam(*env, CPX_PARAM_PARALLELMODE, inst->parallel_mode);

    //CPLEX verbose mode
    if (inst->verbose > 6)
        CPXsetintparam(*env, CPX_PARAM_SCRIND, CPX_ON);

    return status;
}

int TSPopt(int argc, char **argv)
{
    char solver_type[500];
    strcpy(solver_type, "NULL");

    for (int i = 1; i < argc; i++)
        if (strcmp(argv[i], "-solver_type") == 0)
            strcpy(solver_type, argv[++i]);

    if (strcmp(solver_type, "exact") == 0)
    {
        return TSPopt_exact(argc, argv);
    } else if (strcmp(solver_type, "heuristic") == 0)
    {
        return TSPopt_heuristic(argc, argv);
    } else if (strcmp(solver_type, "matheuristic") == 0)
    {
        return TSPopt_matheuristic(argc, argv);
    } else
    {
        print_error("ERROR: Solver type not valid (exact, heuristic or matheuristic should be provided");
    }


    return 0;
}

int TSPopt_heuristic(int argc, char **argv)
{
    simulated_instance *sim_inst = create_sim_inst();
    tour *tour_inst = create_tour();

    read_command_line_heuristic(argc, argv, tour_inst);

    if (!strcmp(tour_inst->heuristic_approach, "VNS"))
    {
        VNS(tour_inst);
    } else if (!strcmp(tour_inst->heuristic_approach, "simulated_annealing"))
    {
        read_command_line_simulated_annealing(argc, argv, sim_inst);
        solve_simulated_annealing(sim_inst);
    } else if (!strcmp(tour_inst->heuristic_approach, "2_opt"))
    {
        solve_heuristic_tsp_mono(tour_inst);
    } else if (!strcmp(tour_inst->heuristic_approach, "multi"))
    {
        solve_heuristic_tsp_multi(tour_inst);
    }

    free_tour(tour_inst);
    free_simulated_instance(sim_inst);

    return 0;
}

int TSPopt_matheuristic(int argc, char **argv)
{
    simulated_instance *sim_inst = create_sim_inst();
    tour *tour_inst = create_tour();
    instance *inst = create_instance();

    read_command_line_matheuristic(argc, argv, sim_inst, tour_inst, inst);
    read_command_line_simulated_annealing(argc, argv, sim_inst);

    CPXENVptr env;
    CPXLPptr lp;

    //Divide the timelimit between the heuristic and the exact part
    tour_inst->timelimit = inst->timelimit * inst->perc_timelimit_ws;
    sim_inst->timelimit = inst->timelimit * inst->perc_timelimit_ws;
    inst->timelimit -= tour_inst->timelimit;

    if (initialize_Cplex(&env, &lp, inst))
        print_error("Wrong Cplex initialization");

    build_model(inst, env, lp);
    initialize_instance(env, lp, inst);

    int status;

    //Different ways to generate the warm start solution
    if (!strcmp(inst->heuristic_approach, "VNS"))
    {
        status = VNS(tour_inst);
    } else if (!strcmp(inst->heuristic_approach, "simulated_annealing"))
    {
        inst->wall_start = wallStart();

        status = solve_simulated_annealing_tour(sim_inst, tour_inst);
    } else if (!strcmp(inst->heuristic_approach, "2-opt"))
    {
        status = solve_heuristic_tsp_mono(tour_inst);
    } else if (!strcmp(inst->heuristic_approach, "multi"))
    {
        status = solve_heuristic_tsp_multi(tour_inst);

    } else
    {
        status = -1;
        print_error("No correct solution type provided.");
    }

    if (status != 0)
        print_error("Wrong generation of the warm start solution.\n");

    //Print stats for blade run
    printf("\nCOST OF THE WARM START SOLUTION\n");
    printf("\nSTAT; HEUR_SOL; %f;\n\n", tour_inst->cost);

    if (!strcmp(inst->solutionapproach, "hard_fixing"))
    {
        status = hard_fixing(env, lp, tour_inst, inst);
    } else if (!strcmp(inst->solutionapproach, "local_branching"))
    {
        status = local_branching(env, lp, tour_inst, inst);
    } else
    {
        status = -1;
        print_error("No correct solution type provided.");
    }

    //Print stats for blade run
    printf("\n\nSTAT; BEST_SOL; %f;\n", inst->best_val);

    free_instance(inst);
    free_tour(tour_inst);
    free_simulated_instance(sim_inst);

    CPXfreeprob(env, &lp);
    CPXcloseCPLEX(&env);

    return status;
}


int TSPopt_exact(int argc, char **argv)
{
    instance *inst = create_instance();
    read_command_line(argc, argv, inst);

    CPXENVptr env;
    CPXLPptr lp;
    if (initialize_Cplex(&env, &lp, inst))
        print_error("Wrong Cplex initialization");

    int status;

    if (!strcmp(inst->solutionapproach, "no_subtour"))
    {
        status = no_subtour(env, lp, inst);
    } else if (!strcmp(inst->solutionapproach, "loop_method"))
    {
        status = simple_loop_method(env, lp, inst);
    } else if (!strcmp(inst->solutionapproach, "weak_loop_method"))
    {
        status = weak_loop_method(env, lp, inst);
    } else if (!strcmp(inst->solutionapproach, "heuristic_loop_method"))
    {
        status = heuristic_loop_method(env, lp, inst);
    } else if (!strcmp(inst->solutionapproach, "lazy_callback"))
    {
        status = lazy_constraint_method(env, lp, inst, 1);
    } else if (!strcmp(inst->solutionapproach, "usercut_callback"))
    {
        status = user_cut_method(env, lp, inst);
    } else
    {
        status = -1;
        print_error("No correct solution type provided.");
    }

    if (status)
        print_error("Error during the execution of the solver");

    if (inst->verbose > 3)
    {
        char plot_title[300];
        sprintf(plot_title, "TSP (%s) - Final cost: %3.0f", inst->solutionapproach, inst->best_val);
        gnuprint(inst->gnuplotPipe, plot_title, inst);
    }


    /*------------------------------------------------------------
     * 4) Final steps
    /*------------------------------------------------------------*/

    //Obtain the best solution found
    status = get_current_sol(env, lp, inst);

    char sol[300];
    solution_code(status, sol);
    printf("Solution: %s\n", sol);

    CPXfreeprob(env, &lp);
    CPXcloseCPLEX(&env);

    free_instance(inst);

    return status;
}