/****************************************
*   UNIVERSITY OF PADOVA                *
*                                       *
*   Operational Research 2 Project      *
*   Travelling Salesman Problem         *
*                                       *
*   Luca Borin                          *
*   Student ID: 1134473                 *
*   Email: borin.luca.93@gmail.com      *
*                                       *
*   Iacopo Mandatelli                   *
*   Student ID: 1151791                  *
*   Email: mandamondo@gmail.com         *
*                                       *
*****************************************/

#include "utilities.h"

void print_error(const char *err)
{
    printf("\n\n ERROR: %s \n\n", err);
    fflush(NULL);
    exit(1);
}

clock_t clockStart()
{
    clock_t cpu_start = clock();
    return (cpu_start);
}

double clockEnd(clock_t cpu_start)
{
    clock_t cpu_end = clock();
    return (((double) (cpu_end - cpu_start)) / CLOCKS_PER_SEC);
}

struct timespec wallStart()
{
    struct timespec wall_start;
    clock_gettime(CLOCK_REALTIME, &wall_start);
    return (wall_start);
}

double wallEnd(struct timespec wall_start)
{
    struct timespec wall_end;
    clock_gettime(CLOCK_REALTIME, &wall_end);
    return ((wall_end.tv_sec + wall_end.tv_nsec * 1e-9) -
            (wall_start.tv_sec + wall_start.tv_nsec * 1e-9));
}

void solution_code(int code, char *explanation)
{
    switch (code)
    {
        case 101:
            strcpy(explanation, "CPXMIP_OPTIMAL: Optimal integer solution found.");
            break;

        case 102:
            strcpy(explanation, "CPXMIP_OPTIMAL_TOL: Optimal sol. within epgap or epagap tolerance found.");
            break;

        case 103:
            strcpy(explanation, "CPXMIP_INFEASIBLE:  Integer infeasible.");
            break;

        case 104:
            strcpy(explanation, "CPXMIP_SOL_LIM: Mixed integer solutions limit exceeded.");
            break;

        case 107:
            strcpy(explanation, "CPXMIP_TIME_LIM_FEAS: Time limit exceeded, integer solution exists.");
            break;

        case 108:
            strcpy(explanation, "CPXMIP_TIME_LIM_INFEAS: Time limit exceeded, no integer solution.");
            break;

        default:
            sprintf(explanation, "Check documentation 'Solution Status Codes' - CODE %d", code);
    }
}

int factorial(int num)
{
    int prod = 1;

    for (int i = 1; i <= num; ++i)
        prod *= i;

    return prod;
}


int comb(int n, int k)
{

    int num = 1;
    int den = 1;

    for (int i = n - k + 1; i <= n; ++i)
        num *= i;

    for (int i = 1; i <= k; ++i)
        den *= i;

    return num / den;
}