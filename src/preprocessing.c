/****************************************
*   UNIVERSITY OF PADOVA                *
*                                       *
*   Operational Research 2 Project      *
*   Travelling Salesman Problem         *
*                                       *
*   Luca Borin                          *
*   Student ID: 1134473                 *
*   Email: borin.luca.93@gmail.com      *
*                                       *
*   Iacopo Mandatelli                   *
*   Student ID: 1151791                  *
*   Email: mandamondo@gmail.com         *
*                                       *
*****************************************/

#include "preprocessing.h"

void read_command_line(int argc, char **argv, instance *inst)
{
    int help = 0;

    if (argc < 1)
        help = 1;

    for (int i = 1; i < argc; i++)
    {

        if (strcmp(argv[i], "-solutionfile") == 0)
        {
            strcpy(inst->solutionfile, argv[++i]);
            continue;
        }

        if (strcmp(argv[i], "-plotfile") == 0)
        {
            strcpy(inst->plotfile, argv[++i]);
            continue;
        }

        if (strcmp(argv[i], "-file") == 0)
        {
            strcpy(inst->inputfile, argv[++i]);
            continue;
        }

        if (strcmp(argv[i], "-inputfile") == 0)
        {
            strcpy(inst->inputfile, argv[++i]);
            continue;
        }

        if (strcmp(argv[i], "-input") == 0)
        {
            strcpy(inst->inputfile, argv[++i]);
            continue;
        }

        if (strcmp(argv[i], "-f") == 0)
        {
            strcpy(inst->inputfile, argv[++i]);
            continue;
        }

        if (strcmp(argv[i], "-solutionapproach") == 0)
        {
            strcpy(inst->solutionapproach, argv[++i]);
            continue;
        }

        if (strcmp(argv[i], "-heuristic_approach") == 0)
        {
            strcpy(inst->heuristic_approach, argv[++i]);
            continue;
        }

        if (strcmp(argv[i], "-printsolutiongap") == 0)
        {
            strcpy(inst->printsolutiongap, argv[++i]);
            continue;
        }

        if (strcmp(argv[i], "-heuristic_warm_start") == 0)
        {
            strcpy(inst->heuristic_warm_start, argv[++i]);
            continue;
        }

        if (strcmp(argv[i], "-time_limit") == 0)
        {
            inst->timelimit = atof(argv[++i]);
            continue;
        }

        if (strcmp(argv[i], "-perc_timelimit_ws") == 0)
        {
            inst->perc_timelimit_ws = atof(argv[++i]);
            continue;
        }

        if (strcmp(argv[i], "-verbose") == 0)
        {
            inst->verbose = atoi(argv[++i]);
            continue;
        }

        if (strcmp(argv[i], "-optimumgap") == 0)
        {
            inst->opt_gap = atof(argv[++i]);
            continue;
        }

        if (strcmp(argv[i], "-closestedges") == 0)
        {
            inst->heur_num_shortest_edges = atoi(argv[++i]);
            continue;
        }

        if (strcmp(argv[i], "-random_seed") == 0)
        {
            inst->random_seed = atoi(argv[++i]);
            continue;
        }

        if (strcmp(argv[i], "-parallel_mode") == 0)
        {
            inst->parallel_mode = atoi(argv[++i]);
            continue;
        }

        if (strcmp(argv[i], "-help") == 0)
        {
            help = 1;
            continue;
        }

        if (strcmp(argv[i], "--help") == 0)
        {
            help = 1;
            continue;
        }

    }

    read_problem(inst->inputfile, inst->prob);

    if (help || (inst->verbose >= 1))        // print current parameters
    {
        printf("\n\navailable parameters --------------------------------------------------\n");
        printf("-file %s\n", inst->inputfile);
        printf("-plotfile %s\n", inst->plotfile);
        printf("-solutionfile %s\n", inst->solutionfile);
        printf("-gnuplotfile %s\n", inst->gnuplotfile);
        printf("-solutionapproach %s\n", inst->solutionapproach);
        printf("-heuristic_warm_start %s\n", inst->heuristic_warm_start);
        printf("-printsolutiongap %s\n", inst->printsolutiongap);
        printf("-time_limit %lf\n", inst->timelimit);
        printf("-perc_timelimit_ws %lf\n", inst->perc_timelimit_ws);
        printf("-verbose %d\n", inst->verbose);
        printf("-optimumgap %f\n", inst->opt_gap);
        printf("-closestedges %d\n", inst->heur_num_shortest_edges);
        printf("-random_seed %d\n", inst->random_seed);
        printf("-parallel_mode %d\n", inst->parallel_mode);
        printf("\nenter -help or --help for help\n");
        printf("----------------------------------------------------------------------------------------------\n\n");
    }

    if (help)
    { exit(1); }
}

void read_problem(char *input_file, problem *prob)
{
    FILE *file = fopen(input_file, "r");

    if (file == NULL)
        print_error(" input file not found!");

    char line[500];
    char *token;

    fgets(line, sizeof(line), file);

    while (strcmp(line, "NODE_COORD_SECTION\n") != 0)
    {
        token = strtok(line, ": ");

        if (strcmp(token, "DIMENSION") == 0)
        {
            token = strtok(NULL, ": \n");
            prob->nnodes = atoi(token);
        } else if (strcmp(token, "EDGE_WEIGHT_TYPE") == 0)
        {
            token = strtok(NULL, ": \n");

            strcpy(prob->weight_type, token);
        }

        fgets(line, sizeof(line), file);
    }

    //Reserve enough memory for the input points
    prob->coord = (point *) malloc(prob->nnodes * sizeof(point));

    int counter = 0;
    double x, y;

    //Parse the input and store the coordinates of the points
    for (int i = 0; i < prob->nnodes; ++i)
    {
        fscanf(file, "%d %lf %lf\n", &counter, &x, &y);
        prob->coord[counter - 1].x = x;
        prob->coord[counter - 1].y = y;
    }

    //Check if EOF reached
    fgets(line, sizeof(line), file);

    if (strncmp("EOF", line, 3) != 0)
    {
        sprintf(line,
                "EOF not reached. Incompatible DIMENSION (%d)vs number of provided points.\n",
                prob->nnodes);
        print_error(line);
    }

    fclose(file);
}

int read_command_line_heuristic(int argc, char **argv, tour *inst_tour)
{
    int help = 0;

    if (argc < 1)
        help = 1;

    for (int i = 1; i < argc; i++)
    {
        if (strcmp(argv[i], "-inputfile") == 0)
        {
            strcpy(inst_tour->inputfile, argv[++i]);
            continue;
        }

        if (strcmp(argv[i], "-time_limit") == 0)
        {
            inst_tour->timelimit = atof(argv[++i]);
            continue;
        }

        if (strcmp(argv[i], "-probability") == 0)
        {
            inst_tour->probability = atof(argv[++i]);
            continue;
        }

        if (strcmp(argv[i], "-solutionapproach") == 0)
        {
            strcpy(inst_tour->solutionapproach, argv[++i]);
            continue;
        }

        if (strcmp(argv[i], "-heuristic_approach") == 0)
        {
            strcpy(inst_tour->heuristic_approach, argv[++i]);
            continue;
        }

        if (strcmp(argv[i], "-verbose") == 0)
        {
            inst_tour->verbose = atoi(argv[++i]);
            continue;
        }

        if (strcmp(argv[i], "-random_seed") == 0)
        {
            inst_tour->random_seed = atoi(argv[++i]);
            continue;
        }
    }

    //Read the input problem
    read_problem(inst_tour->inputfile, inst_tour->instance_problem);
    inst_tour->tour_nodes = (node *) malloc(inst_tour->instance_problem->nnodes * sizeof(node));

    for (int i = 0; i < inst_tour->instance_problem->nnodes; ++i)
    {
        inst_tour->tour_nodes[i].val = -1;
    }

    if (help || (inst_tour->verbose >= 1))        // print current parameters
    {
        printf("\n\nAvailable parameters --------------------------------------------------\n");
        printf("-inputfile %s\n", inst_tour->inputfile);
        printf("-solutionapproach %s\n", inst_tour->solutionapproach);
        printf("-verbose %d\n", inst_tour->verbose);
        printf("\nenter -help or --help for help\n");
        printf("----------------------------------------------------------------------------------------------\n\n");
    }

    if (help)
        exit(1);

    return 0;
}

int read_command_line_simulated_annealing(int argc, char **argv, simulated_instance *inst)
{
    int help = 0;

    if (argc < 1)
        help = 1;

    for (int i = 1; i < argc; i++)
    {
        if (strcmp(argv[i], "-inputfile") == 0)
        {
            strcpy(inst->inputfile, argv[++i]);
            continue;
        }

        if (strcmp(argv[i], "-time_limit") == 0)
        {
            inst->timelimit = atof(argv[++i]);
            continue;
        }

        if (strcmp(argv[i], "-verbose") == 0)
        {
            inst->verbose = atoi(argv[++i]);
            continue;
        }

        if (strcmp(argv[i], "-random_seed") == 0)
        {
            inst->random_seed = atoi(argv[++i]);
            continue;
        }

        if (strcmp(argv[i], "-solutionapproach") == 0)
        {
            strcpy(inst->solutionapproach, argv[++i]);
            continue;
        }

        if (strcmp(argv[i], "-heuristic_approach") == 0)
        {
            strcpy(inst->heuristic_approach, argv[++i]);
            continue;
        }
    }


    read_problem(inst->inputfile, inst->instance_problem);
    inst->nodes = (int *) malloc(inst->instance_problem->nnodes * sizeof(int));

    //Enumerate the indexing array
    for (int i = 0; i < inst->instance_problem->nnodes; ++i)
        inst->nodes[i] = i;

    if (help)
        exit(1);

    return 0;
}

int read_command_line_matheuristic(int argc, char **argv, simulated_instance *sim_inst, tour *tour_inst,
                                   instance *inst)
{
    int help = 0;

    if (argc < 1)
        help = 1;

    read_command_line_simulated_annealing(argc, argv, sim_inst);

    read_command_line_heuristic(argc, argv, tour_inst);

    read_command_line(argc, argv, inst);

    if (help)
        exit(1);

    return 0;
}

instance *create_instance()
{
    instance *inst = (instance *) malloc(sizeof(instance));
    inst->prob = (problem *) malloc(sizeof(problem));
    inst->prob->nnodes = -1;
    inst->prob->coord = NULL;
    strcpy(inst->prob->weight_type, "");
    inst->heur_shortest_edges = NULL;
    inst->heur_num_shortest_edges = -1;
    inst->opt_gap = -1;
    inst->best_sol = NULL;
    inst->best_lb = CPX_INFBOUND;
    inst->best_val = CPX_INFBOUND;
    inst->ncols = -1;
    inst->connected_vertex = NULL;
    inst->loop_iterations = -1;
    inst->n_threads = -1;
    inst->random_seed = -1;
    inst->parallel_mode = -1;
    inst->time_lazy = NULL;
    inst->time_usercut = NULL;
    inst->cpu_start = -1;
    inst->verbose = -1;
    inst->timelimit = CPX_INFBOUND;
    inst->perc_timelimit_ws = -1;
    strcpy(inst->inputfile, "");
    strcpy(inst->plotfile, "");
    strcpy(inst->solutionfile, "");
    strcpy(inst->gnuplotfile, "");
    strcpy(inst->solutionapproach, "");
    strcpy(inst->heuristic_approach, "");
    strcpy(inst->printsolutiongap, "");
    strcpy(inst->heuristic_warm_start, "");
    inst->gnuplotPipe = NULL;
    inst->gnuplotBoundPipe = NULL;

    return inst;
}

tour *create_tour()
{
    tour *inst_tour = (tour *) malloc(sizeof(tour));
    inst_tour->instance_problem = (problem *) malloc(sizeof(problem));
    inst_tour->instance_problem->nnodes = -1;
    inst_tour->instance_problem->coord = NULL;
    inst_tour->tour_nodes = NULL;
    inst_tour->cost = HUGE_VAL;
    inst_tour->start_node = -1;
    inst_tour->probability = -1;
    inst_tour->random_seed = 10;
    inst_tour->timelimit = CPX_INFBOUND;
    inst_tour->verbose = 3;
    inst_tour->gnuplotPipe = NULL;
    strcpy(inst_tour->inputfile, "");
    strcpy(inst_tour->solutionapproach, "");
    strcpy(inst_tour->heuristic_approach, "");

    return inst_tour;
}

simulated_instance *create_sim_inst()
{
    simulated_instance *sim_inst = (simulated_instance *) malloc(sizeof(simulated_instance));
    sim_inst->instance_problem = (problem *) malloc(sizeof(problem));
    sim_inst->instance_problem->nnodes = -1;
    sim_inst->instance_problem->coord = NULL;
    sim_inst->nodes = NULL;
    sim_inst->cost = HUGE_VAL;
    sim_inst->iterations = 5000000;
    sim_inst->temperature = (double) 1;
    sim_inst->iterations = -1;
    sim_inst->timelimit = CPX_INFBOUND;
    sim_inst->verbose = 3;
    sim_inst->gnuplotPipe = NULL;
    strcpy(sim_inst->solutionapproach, "");
    strcpy(sim_inst->inputfile, "");

    return sim_inst;
}