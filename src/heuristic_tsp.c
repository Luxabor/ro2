/****************************************
*   UNIVERSITY OF PADOVA                *
*                                       *
*   Operational Research 2 Project      *
*   Travelling Salesman Problem         *
*                                       *
*   Luca Borin                          *
*   Student ID: 1134473                 *
*   Email: borin.luca.93@gmail.com      *
*                                       *
*   Iacopo Mandatelli                   *
*   Student ID: 1151791                  *
*   Email: mandamondo@gmail.com         *
*                                       *
*****************************************/

#include "heuristic_tsp.h"


/***********************************************
 * NEAREST NEIGHBOR
 **********************************************/

int nearest_neighbor(tour *inst_tour, int start_node, unsigned int *seed)
{
    inst_tour->cost = 0;
    int num_nodes = inst_tour->instance_problem->nnodes;
    if (start_node >= num_nodes)
        return -1;

    int current_node = start_node;

    for (int i = 0; i < num_nodes; ++i)
    {
        if (i == 0)
            inst_tour->tour_nodes[current_node].val = current_node;

        int next_node = nearest_vertex(inst_tour, current_node, start_node, inst_tour->probability, seed);

        inst_tour->tour_nodes[current_node].next = &inst_tour->tour_nodes[next_node];
        inst_tour->tour_nodes[next_node].prev = &inst_tour->tour_nodes[current_node];
        inst_tour->tour_nodes[next_node].val = next_node;

        inst_tour->cost += distance(inst_tour, current_node, next_node);
        current_node = next_node;
    }

    return 0;
}

int nearest_vertex(tour *inst_tour, int origin_vertex, int start_node, double probability, unsigned int *seed)
{
    int num_nodes = inst_tour->instance_problem->nnodes;
    if ((origin_vertex < 0) || (origin_vertex >= num_nodes)) return -1;
    if ((start_node < 0) || (start_node >= num_nodes)) return -1;

    bool found = false;
    double min_distance[3] = {1e70, 1e70, 1e70};
    int min_vertex[3] = {-1, -1, -1};
    for (int i = 0; i < num_nodes; ++i)
    {
        if ((inst_tour->tour_nodes[i].val == -1) && (i != origin_vertex))
        {
            double dist = distance(inst_tour, origin_vertex, i);
            if (dist < min_distance[0])
            {
                min_distance[2] = min_distance[1];
                min_distance[1] = min_distance[0];
                min_distance[0] = dist;
                min_vertex[2] = min_vertex[1];
                min_vertex[1] = min_vertex[0];
                min_vertex[0] = i;
                found = true;
            } else if (dist < min_distance[1])
            {
                min_distance[2] = min_distance[1];
                min_distance[1] = dist;
                min_vertex[2] = min_vertex[1];
                min_vertex[1] = i;
                found = true;
            } else if (dist < min_distance[1])
            {
                min_distance[2] = dist;
                min_vertex[2] = i;
                found = true;
            }
        }
    }

    if (!found)
    {
        return start_node;
    }

    if (probability >= 0.99) return min_vertex[0];


    if (((rand_r(seed) % 100) <= (probability * 100)) || (min_vertex[2] == -1))
    {
        return min_vertex[0];
    } else
    {
        if ((rand_r(seed) % 10) < 5)
            return min_vertex[2];
        else
            return min_vertex[1];
    }
}

/***********************************************
 * MULTISTART
 **********************************************/
int solve_heuristic_tsp_multi(tour *best_tour)
{
    struct timespec wall_start = wallStart();      //instant of start for the wall clock
    int num_threads = omp_get_num_procs();

    tour **thread_local = (tour **) malloc(num_threads * sizeof(tour *));
    tour **thread_best = (tour **) malloc(num_threads * sizeof(tour *));

    for (int i = 0; i < num_threads; ++i)
    {
        thread_local[i] = (tour *) malloc(sizeof(tour));
        thread_best[i] = (tour *) malloc(sizeof(tour));

        copy_instance(best_tour, thread_local[i]);
        copy_instance(best_tour, thread_best[i]);

        if (thread_local[i]->verbose > 3)
            thread_local[i]->gnuplotPipe = popen("gnuplot -persistent", "w"); // -geometry -0-0
    }


    int nnodes = thread_local[0]->instance_problem->nnodes;


#pragma omp parallel num_threads(num_threads)
    {
        unsigned int seed = 42589 + 17 * omp_get_thread_num();
        unsigned int start_seed = 51637 + 1303 * omp_get_thread_num();

        int thread_id = omp_get_thread_num();

        while ((wallEnd(wall_start) < best_tour->timelimit))
        {
            clear_tour(thread_local[thread_id]);

            thread_local[thread_id]->start_node = rand_r(&start_seed) % nnodes; //CHECK

            nearest_neighbor(thread_local[thread_id], thread_local[thread_id]->start_node, &seed);

            opt_2(thread_local[thread_id], 0);

            if (thread_local[thread_id]->cost < thread_best[thread_id]->cost)
            {
                copy_tour(thread_local[thread_id], thread_best[thread_id]);

                if (thread_local[thread_id]->verbose > 3)
                {
                    char plot_title[300];
                    sprintf(plot_title, "HEURISTIC_TSP - Thread %d - Current cost: %3.0f", thread_id,
                            thread_local[thread_id]->cost);
                    heuristic_gnuprint(thread_local[thread_id], plot_title);
                }
            }
        }
    }

    int best = -1;
    double best_cost = HUGE_VAL;

    for (int i = 0; i < num_threads; ++i)
    {
        printf("BEST TOUR COST (thread %d): %f\n", i, thread_best[i]->cost);

        if (thread_best[i]->cost < best_cost)
        {
            best_cost = thread_best[i]->cost;
            best = i;
        }
    }

    copy_tour(thread_best[best], best_tour);

    if (best_tour->verbose > 3)
    {
        best_tour->gnuplotPipe = popen("gnuplot -persistent", "w");
        char plot_title[300];
        sprintf(plot_title, "BEST HEURISTIC_TSP - Cost: %3.0f", best_tour->cost);
        heuristic_gnuprint(best_tour, plot_title);
        pclose(best_tour->gnuplotPipe);
    }

    printf("BEST TOUR TOTAL: %f\n", best_tour->cost);

    for (int i = 0; i < num_threads; ++i)
    {
        if (thread_local[i]->verbose > 3)
            pclose(thread_local[i]->gnuplotPipe);
        free_tour(thread_local[i]);
        free_tour(thread_best[i]);
    }

    printf("Time elapsed: %f\n", wallEnd(wall_start));

    return 0;
}

int solve_heuristic_tsp_mono(tour *best_tour)
{
    best_tour->wall_start = wallStart();

    srand((unsigned) best_tour->random_seed);

    unsigned int seed_nearest_neighbor = 42589;

    tour *local_instance = create_tour();
    copy_instance(best_tour, local_instance);

    while ((wallEnd(best_tour->wall_start) < best_tour->timelimit))
    {
        clear_tour(local_instance);

        local_instance->start_node = (rand() % local_instance->instance_problem->nnodes);

        nearest_neighbor(local_instance, local_instance->start_node, &seed_nearest_neighbor);

        opt_2(local_instance, 0);

        if (local_instance->cost < best_tour->cost)
        {
            copy_tour(local_instance, best_tour);
            printf("BEST TOUR COST: %f\n", best_tour->cost);
        }
    }

    free_tour(local_instance);

    return 0;
}

/***********************************************
 * 2-OPT FUNCTIONS
 **********************************************/

int opt_2(tour *inst_tour, int type)
{
    int iter = 0;

    if (type == 0)
    {
        while (opt_2_first_swap(inst_tour) == 1 && iter < 10000)
        {
            iter++;
        }
    } else
    {
        while (opt_2_best_swap(inst_tour) == 1 && iter < 10000)
        {
            iter++;
        }
    }

    return iter;
}

int opt_2_first_swap(tour *inst_tour)
{
    int updated = 0;

    double new_cost, old_cost;
    int temp;

    //Start node of the cycle
    int start = inst_tour->start_node;

    int begin = inst_tour->start_node;
    int next_node = inst_tour->tour_nodes[start].next->val;

    while (next_node != begin && updated == 0)
    {
        int current_out = next_node;
        int from_out = start;
        int current = next_node;
        int from = start;


        while (current != begin && updated == 0)
        {
            temp = current;
            current = select_tour_edge(inst_tour->tour_nodes, from, current);
            from = temp;

            old_cost = distance(inst_tour, from_out, current_out) +
                       distance(inst_tour, from, current);
            new_cost = distance(inst_tour, from_out, from) +
                       distance(inst_tour, current_out, current);

            if (new_cost < old_cost)
            {
                swap(inst_tour->tour_nodes, from_out, current_out, from, current);
                inst_tour->cost += new_cost - old_cost;
                updated = 1;
            }

        }

        temp = next_node;
        next_node = select_tour_edge(inst_tour->tour_nodes, start, next_node);
        start = temp;
    }

    return updated;
}

int opt_2_best_swap(tour *inst_tour)
{
    double new_cost, old_cost;
    int temp;

    //Start node of the cycle
    int start_node = inst_tour->start_node;

    //Next node of the
    int next_node = inst_tour->tour_nodes[start_node].next->val; //CHECK

    double best_red = -1;
    int best_start, best_next, best_from, best_current;

    while (next_node != inst_tour->start_node)
    {
        int from = next_node;
        int current = select_tour_edge(inst_tour->tour_nodes, start_node, from);

        while (current != inst_tour->start_node)
        {
            temp = current;
            current = select_tour_edge(inst_tour->tour_nodes, from, current);
            from = temp;

            old_cost = distance(inst_tour, start_node, next_node) +
                       distance(inst_tour, from, current);
            new_cost = distance(inst_tour, start_node, from) +
                       distance(inst_tour, next_node, current);

            if (new_cost < old_cost && best_red < old_cost - new_cost)
            {
                best_red = old_cost - new_cost;
                best_start = start_node;
                best_next = next_node;
                best_from = from;
                best_current = current;
            }

        }

        temp = next_node;
        next_node = select_tour_edge(inst_tour->tour_nodes, start_node, next_node);
        start_node = temp;
    }


    if (best_red != -1)
    {
        swap(inst_tour->tour_nodes, best_start, best_next, best_from, best_current);
        inst_tour->cost -= best_red;
        return 1;
    }

    return 0;
}

/***********************************************
 * VNS
 **********************************************/

int VNS(tour *best_tour)
{
    best_tour->wall_start = wallStart();      //instant of start for the wall clock

    best_tour->cost = HUGE_VAL;
    int random_seed = best_tour->random_seed;
    srand(random_seed);


    //Initialize local instance
    tour *local_instance = (tour *) malloc(sizeof(tour));

    copy_instance(best_tour, local_instance);
    local_instance->start_node = (rand() % local_instance->instance_problem->nnodes);

    //Get starting random tour
    nearest_neighbor(local_instance, local_instance->start_node, &random_seed);

    //Create gnuplot pipes
    if (local_instance->verbose > 3)
    {
        local_instance->gnuplotPipe = popen("gnuplot -persistent", "w");
        best_tour->gnuplotPipe = popen("gnuplot -persistent", "w");
    }

    char plot_title[300];

    while (wallEnd(best_tour->wall_start) < best_tour->timelimit)
    {
        //Perform 2opt optimization
        opt_2(local_instance, 0);

        if (local_instance->verbose > 5)
        {
            sprintf(plot_title, "HEURISTIC_TSP - Current cost: %3.0f", local_instance->cost);
            heuristic_gnuprint(local_instance, plot_title);
        }

        //Update best tour
        if (local_instance->cost < best_tour->cost)
        {
            copy_tour(local_instance, best_tour);

            if (best_tour->verbose > 3)
            {
                printf("BEST TOUR COST: %f\n", best_tour->cost);
                sprintf(plot_title, "HEURISTIC_TSP_BEST - Current cost: %3.0f", best_tour->cost);
                heuristic_gnuprint(best_tour, plot_title);
            }
        }

        //Perform a random 3opt move
        opt_3_perform_move_sorted(local_instance);
    }

    if (best_tour->verbose > 3)
    {
        pclose(best_tour->gnuplotPipe);
        pclose(local_instance->gnuplotPipe);
        best_tour->gnuplotPipe = NULL;
    }

    free_tour(local_instance);


    return 0;
}

void create_sorted_tour(tour *inst_tour, int *sorted_tour)
{
    int temp;
    int from = inst_tour->start_node;
    int current = inst_tour->tour_nodes[from].next->val;

    sorted_tour[from] = current;

    while (current != inst_tour->start_node)
    {
        temp = current;
        current = select_tour_edge(inst_tour->tour_nodes, from, current);
        from = temp;
        sorted_tour[from] = current;
    }
}

void create_sorted_tour_annealing(tour *inst_tour, int *sorted_tour)
{
    node current_node;
    for (int i = 0; i < inst_tour->instance_problem->nnodes; i++)
    {
        if (i == 0)
            current_node = inst_tour->tour_nodes[inst_tour->start_node];
        node next_node = *current_node.next;
        sorted_tour[i] = current_node.val;
        current_node = next_node;
    }
}

void create_ordered_tour(int *sorted_tour, int start_node, int nnodes)
{
    int tmp[nnodes];

    int next = start_node;
    tmp[0] = next + 1;
    for (int i = 1; i < nnodes; ++i)
    {
        tmp[i] = sorted_tour[next] + 1;
        next = sorted_tour[next];
    }

    for (int i = 0; i < nnodes; ++i)
    {
        sorted_tour[i] = tmp[i] - 1;
    }
}

void opt_3_find_valid_edges_sorted(int *sorted_edges, int nnodes, int *edges)
{
    edges[0] = (rand() % nnodes);
    edges[1] = sorted_edges[edges[0]];
    edges[2] = (rand() % nnodes);
    edges[3] = sorted_edges[edges[2]];
    edges[4] = (rand() % nnodes);
    edges[5] = sorted_edges[edges[4]];
}

void opt_3_perform_move_sorted(tour *inst_tour)
{
    int *sorted_tour_next = (int *) malloc(inst_tour->instance_problem->nnodes * sizeof(int));
    int nnodes = inst_tour->instance_problem->nnodes;

    //Create a sorted representation of the tour
    create_sorted_tour(inst_tour, sorted_tour_next);

    //Select 3 valid random edges to swap
    int random_edges[6];
    do
    {
        opt_3_find_valid_edges_sorted(sorted_tour_next, nnodes, random_edges);
    } while (opt_3_check_edges(random_edges) != 0);

    //Sort the edges in the same order of the tour
    sort_tour_edges(sorted_tour_next, random_edges);

    //Perform 3opt swap and update the tour cost
    inst_tour->cost += swap_opt_3(inst_tour, 0, 1, 2, 3, 4, 5, random_edges);
}

void opt_3_find_valid_edges_not_sorted(tour *inst_tour, int *edges)
{
    edges[0] = (rand() % inst_tour->instance_problem->nnodes);
    edges[1] = inst_tour->tour_nodes[edges[0]].next->val;
    edges[2] = (rand() % inst_tour->instance_problem->nnodes);
    edges[3] = inst_tour->tour_nodes[edges[2]].next->val;
    edges[4] = (rand() % inst_tour->instance_problem->nnodes);
    edges[5] = inst_tour->tour_nodes[edges[4]].next->val;
}

void opt_3_perform_move_not_sorted(tour *inst_tour)
{
    int random_edges[6];

    do
    {
        opt_3_find_valid_edges_not_sorted(inst_tour, random_edges);
    } while (opt_3_check_edges(random_edges) != 0);


    int swap_done = 0;
    int swap_type = 0;

    while (swap_done == 0)
    {
        if (swap_type == 0)
        {
            inst_tour->cost += swap_opt_3(inst_tour, 0, 1, 2, 3, 4, 5, random_edges);

            if (opt_3_count_tour_edges_not_sorted(inst_tour) == inst_tour->instance_problem->nnodes)
            {
                if (inst_tour->verbose > 4)
                {
                    printf("SWAP TYPE DONE: %d\n", swap_type);
                    printf("NEW TOUR COST: %f\n", inst_tour->cost);
                }

                swap_done = 1;
            } else
            {
                if (inst_tour->verbose > 4)
                {
                    printf("REVERSE TYPE DONE: %d\n", swap_type);
                    printf("NEW TOUR COST: %f\n", inst_tour->cost);
                }
                inst_tour->cost += reverse_swap_opt_3(inst_tour, 0, 1, 2, 3, 4, 5, random_edges);
                ++swap_type;
            }

        } else if (swap_type == 1)
        {
            inst_tour->cost += swap_opt_3(inst_tour, 0, 1, 3, 2, 4, 5, random_edges);

            if (opt_3_count_tour_edges_not_sorted(inst_tour) == inst_tour->instance_problem->nnodes)
            {
                if (inst_tour->verbose > 4)
                {
                    printf("SWAP TYPE DONE: %d\n", swap_type);
                    printf("NEW TOUR COST: %f\n", inst_tour->cost);
                }
                swap_done = 1;
            } else
            {
                if (inst_tour->verbose > 4)
                {
                    printf("REVERSE TYPE DONE: %d\n", swap_type);
                    printf("NEW TOUR COST: %f\n", inst_tour->cost);
                }
                inst_tour->cost += reverse_swap_opt_3(inst_tour, 0, 1, 3, 2, 4, 5, random_edges);
                ++swap_type;
            }

        } else if (swap_type == 2)
        {
            inst_tour->cost += swap_opt_3(inst_tour, 1, 0, 2, 3, 4, 5, random_edges);

            if (opt_3_count_tour_edges_not_sorted(inst_tour) == inst_tour->instance_problem->nnodes)
            {
                if (inst_tour->verbose > 4)
                {
                    printf("SWAP TYPE DONE: %d\n", swap_type);
                    printf("NEW TOUR COST: %f\n", inst_tour->cost);
                }
                swap_done = 1;
            } else
            {
                if (inst_tour->verbose > 4)
                {
                    printf("REVERSE TYPE DONE: %d\n", swap_type);
                    printf("NEW TOUR COST: %f\n", inst_tour->cost);
                }
                inst_tour->cost += reverse_swap_opt_3(inst_tour, 1, 0, 2, 3, 4, 5, random_edges);
                ++swap_type;
            }
        } else if (swap_type == 3)
        {
            inst_tour->cost += swap_opt_3(inst_tour, 0, 1, 2, 3, 5, 4, random_edges);

            if (opt_3_count_tour_edges_not_sorted(inst_tour) == inst_tour->instance_problem->nnodes)
            {
                if (inst_tour->verbose > 4)
                {
                    printf("SWAP TYPE DONE: %d\n", swap_type);
                    printf("NEW TOUR COST: %f\n", inst_tour->cost);
                }
                swap_done = 1;
            } else
            {
                if (inst_tour->verbose > 4)
                {
                    printf("REVERSE TYPE DONE: %d\n", swap_type);
                    printf("NEW TOUR COST: %f\n", inst_tour->cost);
                }
                inst_tour->cost += reverse_swap_opt_3(inst_tour, 0, 1, 2, 3, 5, 4, random_edges);
                ++swap_type;
            }
        } else
        {
            do
            {
                opt_3_find_valid_edges_not_sorted(inst_tour, random_edges);
            } while (opt_3_check_edges(random_edges) != 0);

            if (inst_tour->verbose > 4)
            {
                printf("OPT_3 MOVE NOT POSSIBLE. Select new edges.\n");
                for (int i = 0; i < 6; i += 2)
                {
                    printf("%d-%d\n", random_edges[i] + 1, random_edges[i + 1] + 1);
                }

            }


            swap_type = 0;
        }
    }
}

int opt_3_count_tour_edges_not_sorted(tour *inst_tour)
{
    int temp;
    int from = inst_tour->start_node;
    int current = inst_tour->tour_nodes[from].next->val; //CHECK;
    int edges = 0;

    while (current != inst_tour->start_node)
    {
        temp = current;
        current = select_tour_edge(inst_tour->tour_nodes, from, current);
        from = temp;
        ++edges;
    }
    return edges + 1;
}

double swap_opt_3(tour *inst_tour, int a1, int a2, int b1, int b2, int c1, int c2, int *random_edges)
{

    double old_cost = distance(inst_tour, random_edges[a1], random_edges[a2])
                      + distance(inst_tour, random_edges[b1], random_edges[b2])
                      + distance(inst_tour, random_edges[c1], random_edges[c2]);

    double new_cost = distance(inst_tour, random_edges[a1], random_edges[b2])
                      + distance(inst_tour, random_edges[a2], random_edges[c1])
                      + distance(inst_tour, random_edges[b1], random_edges[c2]);

    //Perform the swap
    node **correct_pointer;

    correct_pointer = select_destination_edge(inst_tour->tour_nodes, random_edges[a1], random_edges[a2]);
    *correct_pointer = &(inst_tour->tour_nodes[random_edges[b2]]);
    correct_pointer = select_destination_edge(inst_tour->tour_nodes, random_edges[b2], random_edges[b1]);
    *correct_pointer = &(inst_tour->tour_nodes[random_edges[a1]]);


    correct_pointer = select_destination_edge(inst_tour->tour_nodes, random_edges[a2], random_edges[a1]);
    *correct_pointer = &(inst_tour->tour_nodes[random_edges[c1]]);
    correct_pointer = select_destination_edge(inst_tour->tour_nodes, random_edges[c1], random_edges[c2]);
    *correct_pointer = &(inst_tour->tour_nodes[random_edges[a2]]);


    correct_pointer = select_destination_edge(inst_tour->tour_nodes, random_edges[b1], random_edges[b2]);
    *correct_pointer = &(inst_tour->tour_nodes[random_edges[c2]]);
    correct_pointer = select_destination_edge(inst_tour->tour_nodes, random_edges[c2], random_edges[c1]);
    *correct_pointer = &(inst_tour->tour_nodes[random_edges[b1]]);

    return new_cost - old_cost;
}

double reverse_swap_opt_3(tour *inst_tour, int a1, int a2, int b1, int b2, int c1, int c2, int *random_edges)
{

    double new_cost = distance(inst_tour, random_edges[a1], random_edges[a2])
                      + distance(inst_tour, random_edges[b1], random_edges[b2])
                      + distance(inst_tour, random_edges[c1], random_edges[c2]);

    double old_cost = distance(inst_tour, random_edges[a1], random_edges[b2])
                      + distance(inst_tour, random_edges[a2], random_edges[c2])
                      + distance(inst_tour, random_edges[b1], random_edges[c1]);

    //Perform the swap
    node **correct_pointer;

    correct_pointer = select_destination_edge(inst_tour->tour_nodes, random_edges[a1], random_edges[b2]);
    *correct_pointer = &(inst_tour->tour_nodes[random_edges[a2]]);
    correct_pointer = select_destination_edge(inst_tour->tour_nodes, random_edges[b2], random_edges[a1]);
    *correct_pointer = &(inst_tour->tour_nodes[random_edges[b1]]);


    correct_pointer = select_destination_edge(inst_tour->tour_nodes, random_edges[b1], random_edges[c1]);
    *correct_pointer = &(inst_tour->tour_nodes[random_edges[b2]]);
    correct_pointer = select_destination_edge(inst_tour->tour_nodes, random_edges[c1], random_edges[b1]);
    *correct_pointer = &(inst_tour->tour_nodes[random_edges[c2]]);


    correct_pointer = select_destination_edge(inst_tour->tour_nodes, random_edges[a2], random_edges[c2]);
    *correct_pointer = &(inst_tour->tour_nodes[random_edges[a1]]);
    correct_pointer = select_destination_edge(inst_tour->tour_nodes, random_edges[c2], random_edges[a2]);
    *correct_pointer = &(inst_tour->tour_nodes[random_edges[c1]]);

    return new_cost - old_cost;
}

int opt_3_check_edges(int *edges)
{
    //Avoid equal or adjacent edges
    for (int i = 0; i < 6; i += 2)
    {
        if (count_occurrences(edges[i], edges, 6) != 1)
            return -1;
    }

    return 0;
}

void sort_tour_edges(int *sorted_tour, int *edges)
{
    int done = 0;
    int next = edges[1];

    while (done == 0)
    {
        if (sorted_tour[next] == edges[2])
            done = 1;
        else if (sorted_tour[next] == edges[4])
        {
            int tempA = edges[2];
            int tempB = edges[3];

            edges[2] = edges[4];
            edges[3] = edges[5];
            edges[4] = tempA;
            edges[5] = tempB;
            done = 1;
        }
        next = sorted_tour[next];
    }
}

/***********************************************
 * SIMULATED ANNEALING
 **********************************************/

double distance_simulated_annealing(simulated_instance *inst, int point1, int point2)
{
    if (strcmp(inst->instance_problem->weight_type, "EUC_2D") == 0)
    {
        return ((double) euclidean_distance(inst->instance_problem, point1, point2));
    }
    return -1;
}

void swap_simulated_annealing(int *array, int i, int j)
{
    int temp;
    temp = array[i];
    array[i] = array[j];
    array[j] = temp;
}

double generate_initial_tour(simulated_instance *inst)
{
    //some randomness in the initial tour
    for (int i = 0; i < inst->instance_problem->nnodes; i++)
    {
        int j = rand() % inst->instance_problem->nnodes;
        int k = rand() % inst->instance_problem->nnodes;
        swap_simulated_annealing(inst->nodes, j, k);
    }

    //calculate the cost of the initial tour
    double cost = 0;
    for (int i = 0; i < inst->instance_problem->nnodes; i++)
    {
        if (i != inst->instance_problem->nnodes - 1)
        {
            cost += distance_simulated_annealing(inst, i, i + 1);
        } else
        {
            cost += distance_simulated_annealing(inst, i, 0);
        }
        //printf("[SIM_ANN] Initial node %d \n", inst->nodes[i]);
    }
    printf("[SIM_ANN] The initial cost of the tour is %f \n", cost);
    return cost;
}

double generate_new_tour_1(simulated_instance *inst, int *new_tour)
{
    memcpy(new_tour, inst->nodes, inst->instance_problem->nnodes * sizeof(int));

    //find to points i and j such that i < j < |nodes|
    int i = rand() % (inst->instance_problem->nnodes - 1);
    int j = (rand() % (inst->instance_problem->nnodes - i - 1)) + (i + 1);
    //printf("[SIM_ANN] i is %d and j is %d \n", i, j);

    //reverse the portion of the tour that lies between i and j
    for (int k = 0; k < inst->instance_problem->nnodes; k++)
    {
        if (k < i)
        {
            new_tour[k] = inst->nodes[k];
            //printf("[SIM_ANN] inst->nodes[k] is %d \n", inst->nodes[k]);
        } else if (k <= j)
        {
            new_tour[k] = inst->nodes[j - (k - i)];
        } else
        {
            new_tour[k] = inst->nodes[k];
        }
        //printf("[SIM_ANN] k is %d and new_tour[k] is  %d \n", k, new_tour[k]);
    }

    //calculate the cost of the new tour
    double cost = 0;
    for (int z = 0; z < inst->instance_problem->nnodes; z++)
    {
        if (z != inst->instance_problem->nnodes - 1)
        {
            cost += (double) euclidean_distance(inst->instance_problem, new_tour[z], new_tour[z + 1]);
        } else
        {
            cost += (double) euclidean_distance(inst->instance_problem, new_tour[z], new_tour[0]);
        }
    }

    //printf("[SIM_ANN] The new cost of the tour is %f \n", cost);

    return cost;
}

double generate_new_tour_2(simulated_instance *inst, int *new_tour)
{
    memcpy(new_tour, inst->nodes, inst->instance_problem->nnodes * sizeof(int));

    //do (1, nnodes-1) rand permutations of 2 elements
    int iterations = (rand() % inst->instance_problem->nnodes) + 1;
    for (int i = 0; i < iterations; i++)
    {
        int j = rand() % inst->instance_problem->nnodes;
        int k = rand() % inst->instance_problem->nnodes;
        swap_simulated_annealing(new_tour, j, k);
    }

    //calculate the cost of the new tour
    double cost = 0;
    for (int z = 0; z < inst->instance_problem->nnodes; z++)
    {
        if (z != inst->instance_problem->nnodes - 1)
        {
            cost += (double) euclidean_distance(inst->instance_problem, new_tour[z], new_tour[z + 1]);
        } else
        {
            cost += (double) euclidean_distance(inst->instance_problem, new_tour[z], new_tour[0]);
        }
    }

    //printf("[SIM_ANN] The new cost of the tour is %f \n", cost);

    return cost;
}

int probability(double old_cost, double new_cost, double temperature)
{
    if (temperature < 0)
        return 0;
    if (new_cost < old_cost)
        return 1;
    //double normalize = ((1E-30) > ((new_cost + old_cost) / 2) ? (1E-10) : ((new_cost + old_cost) / 2));
    double normalize = (new_cost + old_cost) / 2;
    //printf("Old cost: %f. New cost: %f. Temperature: %2.11f. Porbability %2.2f. \n", old_cost, new_cost, temperature, (100 * pow(M_E, -(((new_cost - old_cost)) / (normalize * temperature)))));
    if ((rand() % 100) < (100 * pow(M_E, -(((new_cost - old_cost)) / (normalize * temperature)))))
        return 1;
    else
        return 0;
}

int solve_simulated_annealing(simulated_instance *inst)
{
    srand((unsigned) inst->random_seed);

    if (inst->verbose > 3)
        inst->gnuplotPipe = popen("gnuplot -persistent", "w");

    inst->cost = generate_initial_tour(inst);
    inst->wall_start = wallStart();

    int i = 0;
    while ((i < inst->iterations) && (wallEnd(inst->wall_start) < inst->timelimit))
    {
        int *new_tour = (int *) malloc(inst->instance_problem->nnodes * sizeof(int));
        double new_cost = generate_new_tour_1(inst, new_tour);
        //double new_cost = generate_new_tour_2(inst, new_tour);

        if (probability(inst->cost, new_cost, inst->temperature / 1))
        {
            memcpy(inst->nodes, new_tour, inst->instance_problem->nnodes * sizeof(int));
            inst->cost = new_cost;
        }
        free(new_tour);

        double a = (double) 1;
        double b = -3.68414E-6;
        double c = -1E-8;
        double exp = (a * pow((double) M_E, (b * i))) + c;
        inst->temperature = exp;

        //plot the intermediate tour
        if (!(i % 100000) && (inst->verbose > 3))
        {
            char plot_title[300];
            sprintf(plot_title, "CURRENT SIMULATED_ANNEALING_TSP - Temperature: %2.10f - Cost: %3.0f",
                    inst->temperature,
                    inst->cost);
            simulated_annealing_gnuprint(inst, plot_title);
        }

        i++;
    }

    //plot the final tour
    if (inst->verbose > 3)
    {
        char plot_title[300];
        sprintf(plot_title, "CURRENT SIMULATED_ANNEALING_TSP - Temperature: %f - Cost: %3.0f", inst->temperature,
                inst->cost);
        simulated_annealing_gnuprint(inst, plot_title);
        pclose(inst->gnuplotPipe);
    }

    //printf("[SIM_ANN] The final cost of the tour is %f \n", inst->cost);

    return 0;
}

void free_simulated_instance(simulated_instance *inst)
{

    if (!inst->instance_problem)
    {
        if (!inst->instance_problem->coord)
            free(inst->instance_problem->coord);
        inst->instance_problem->coord = NULL;

        free(inst->instance_problem);
    }

    if (!inst->nodes)
        free(inst->nodes);


    inst->instance_problem = NULL;
    inst->nodes = NULL;
    inst->gnuplotPipe = NULL;

    free(inst);
}

void simulated_annealing_gnuprint(simulated_instance *inst, char *title)
{
    fprintf(inst->gnuplotPipe, "set term x11\n");

    fprintf(inst->gnuplotPipe, "clear\n");

    fprintf(inst->gnuplotPipe, "set style line 1 lc rgb '#0060ad' lt 1 lw 2 pt 7 ps 1.5\n");

    fprintf(inst->gnuplotPipe, "set title \"%s\"\n", title);

    fprintf(inst->gnuplotPipe, "plot '-' with linespoints ls 1 notitle\n");

    double x1, x2, y1, y2;
    for (int i = 0; i < inst->instance_problem->nnodes; i++)
    {
        if (i != inst->instance_problem->nnodes - 1)
        {
            x1 = inst->instance_problem->coord[inst->nodes[i]].x;
            y1 = inst->instance_problem->coord[inst->nodes[i]].y;
            x2 = inst->instance_problem->coord[inst->nodes[i + 1]].x;
            y2 = inst->instance_problem->coord[inst->nodes[i + 1]].y;
            fprintf(inst->gnuplotPipe, "%lf %lf\n%lf %lf\n\n", x1, y1, x2, y2);
        } else
        {
            x1 = inst->instance_problem->coord[inst->nodes[i]].x;
            y1 = inst->instance_problem->coord[inst->nodes[i]].y;
            x2 = inst->instance_problem->coord[inst->nodes[0]].x;
            y2 = inst->instance_problem->coord[inst->nodes[0]].y;
            fprintf(inst->gnuplotPipe, "%lf %lf\n%lf %lf\n\n", x1, y1, x2, y2);
        }
    }

    fprintf(inst->gnuplotPipe, "e\n");

    fflush(inst->gnuplotPipe);
}

int solve_simulated_annealing_tour(simulated_instance *sim_inst, tour *inst_tour)
{
    solve_simulated_annealing(sim_inst);

    inst_tour->start_node = sim_inst->nodes[0];
    inst_tour->cost = sim_inst->cost;
    memcpy(inst_tour->solutionapproach, sim_inst->solutionapproach, strlen(sim_inst->solutionapproach) + 1);

    inst_tour->instance_problem = (problem *) malloc(sizeof(problem));
    inst_tour->instance_problem->coord = (point *) malloc(inst_tour->instance_problem->nnodes * sizeof(point));
    inst_tour->instance_problem = sim_inst->instance_problem;
    //memcpy(inst_tour->instance_problem->coord, sim_inst->instance_problem->coord, sim_inst->instance_problem->nnodes * sizeof(point));
    inst_tour->tour_nodes = (node *) malloc(inst_tour->instance_problem->nnodes * sizeof(node));

    //printf("[SIM]");
    //copio la vecchia struttura dati nella nuova
    for (int i = 0; i < inst_tour->instance_problem->nnodes; i++)
    {
        //printf("- %d ", sim_inst->nodes[i]);
        inst_tour->tour_nodes[i].val = sim_inst->nodes[i];
        inst_tour->tour_nodes[i].next = &inst_tour->tour_nodes[(i + 1) % inst_tour->instance_problem->nnodes];
        inst_tour->tour_nodes[i].prev = &inst_tour->tour_nodes[((i - 1) + inst_tour->instance_problem->nnodes) %
                                                               inst_tour->instance_problem->nnodes];
        //printf("node - %d \n", ((i - 1) + inst_tour->instance_problem->nnodes) % inst_tour->instance_problem->nnodes);
    }

    return 0;
}

/***********************************************
 * UTILITY
 **********************************************/

int euclidean_distance(problem *inst, int point1, int point2)
{
    if (point1 == point2)
    { return 0; }
    double first = pow((inst->coord[point1].x - inst->coord[point2].x), (double) 2);
    double second = pow((inst->coord[point1].y - inst->coord[point2].y), (double) 2);
    double ce = sqrt(first + second);
    return (round(ce));
}

double distance(tour *inst, int point1, int point2)
{
    if (strcmp(inst->instance_problem->weight_type, "EUC_2D") == 0)
    {
        return ((double) euclidean_distance(inst->instance_problem, point1, point2));
    }

    return -1;
}

void free_tour(tour *inst_tour)
{
    if (!inst_tour->instance_problem)
    {
        if (!inst_tour->instance_problem->coord)
            free(inst_tour->instance_problem->coord);

        inst_tour->instance_problem->coord = NULL;

        free(inst_tour->instance_problem);
    }

    if (!inst_tour->tour_nodes)
        free(inst_tour->tour_nodes);

    inst_tour->instance_problem = NULL;
    inst_tour->tour_nodes = NULL;
    inst_tour->gnuplotPipe = NULL;

    free(inst_tour);
}

void clear_tour(tour *source)
{
    for (int i = 0; i < source->instance_problem->nnodes; ++i)
        source->tour_nodes[i].val = -1;

    source->cost = -1;
}

void print_tour(tour *inst_tour)
{
    int temp;
    int from = inst_tour->start_node;
    int current = inst_tour->tour_nodes[from].next->val; //CHECK;

    printf("START NODE: %d\n", inst_tour->start_node + 1);

    printf("%d", from + 1);
    while (current != inst_tour->start_node)
    {
        printf("=>");
        temp = current;
        current = select_tour_edge(inst_tour->tour_nodes, from, current);
        from = temp;

        printf("%d", from + 1);
        fflush(stdout);
    }
    printf("\n");
}

void print_tour_sorted(int *sorted_tour, int start_node, int nnodes)
{
    printf("TOUR:\n");

    int next = start_node;
    printf("%d=>", start_node + 1);
    for (int i = 0; i < nnodes; ++i)
    {
        printf("%d", sorted_tour[next] + 1);
        next = sorted_tour[next];
        fflush(stdout);
        if (i != nnodes - 1)
            printf("=>");
    }
    printf("\n");
}

void heuristic_gnuprint(tour *inst_tour, char *title)
{
    fprintf(inst_tour->gnuplotPipe, "set term x11\n");

    fprintf(inst_tour->gnuplotPipe, "clear\n");

    fprintf(inst_tour->gnuplotPipe, "set style line 1 lc rgb '#0060ad' lt 1 lw 2 pt 7 ps 1.5\n");

    fprintf(inst_tour->gnuplotPipe, "set title \"%s\"\n", title);

    fprintf(inst_tour->gnuplotPipe, "plot '-' with linespoints ls 1 notitle\n");

    double x1, x2, y1, y2;
    int temp;
    int from = inst_tour->start_node;
    int current = inst_tour->tour_nodes[from].next->val; //CHECK;

    x1 = inst_tour->instance_problem->coord[from].x;
    y1 = inst_tour->instance_problem->coord[from].y;
    x2 = inst_tour->instance_problem->coord[current].x;
    y2 = inst_tour->instance_problem->coord[current].y;
    fprintf(inst_tour->gnuplotPipe, "%lf %lf\n%lf %lf\n\n", x1, y1, x2, y2);

    while (current != inst_tour->start_node)
    {
        temp = current;
        current = select_tour_edge(inst_tour->tour_nodes, from, current);
        from = temp;

        x1 = inst_tour->instance_problem->coord[from].x;
        y1 = inst_tour->instance_problem->coord[from].y;
        x2 = inst_tour->instance_problem->coord[current].x;
        y2 = inst_tour->instance_problem->coord[current].y;
        fprintf(inst_tour->gnuplotPipe, "%lf %lf\n%lf %lf\n\n", x1, y1, x2, y2);
    }

    fprintf(inst_tour->gnuplotPipe, "e\n");

    fflush(inst_tour->gnuplotPipe);
}

void copy_instance(tour *src_tour, tour *dest_tour)
{
    int nnodes = src_tour->instance_problem->nnodes;

    dest_tour->instance_problem = (problem *) malloc(sizeof(problem));
    dest_tour->tour_nodes = (node *) malloc(nnodes * sizeof(node));
    dest_tour->instance_problem->coord = (point *) malloc(nnodes * sizeof(point));

    dest_tour->cost = src_tour->cost;
    dest_tour->probability = src_tour->probability;
    dest_tour->random_seed = src_tour->random_seed;
    dest_tour->instance_problem->nnodes = nnodes;
    dest_tour->verbose = src_tour->verbose;
    dest_tour->start_node = src_tour->start_node;


    memcpy(dest_tour->tour_nodes, src_tour->tour_nodes, nnodes * sizeof(node));

    memcpy(dest_tour->instance_problem->coord, src_tour->instance_problem->coord, nnodes * sizeof(point));
    memcpy(dest_tour->instance_problem->weight_type, src_tour->instance_problem->weight_type,
           sizeof(src_tour->instance_problem->weight_type));

}

void copy_tour(tour *source, tour *destination)
{
    memcpy(destination->tour_nodes, source->tour_nodes, source->instance_problem->nnodes * sizeof(node));
    destination->cost = source->cost;
    destination->start_node = source->start_node;
}

int find_int(int num, int *array, int size)
{
    for (int i = 0; i < size; ++i)
    {
        if (array[i] == num)
            return i;
    }
    return -1;
}

int count_occurrences(int num, int *array, int size)
{
    int count = 0;

    for (int i = 0; i < size; ++i)
    {
        if (array[i] == num)
            ++count;
    }

    return count;
}

void print_stats(tour *tour_inst, simulated_instance *sim_inst)
{
    if (!strcmp(tour_inst->heuristic_approach, "simulated_annealing"))
    {
        printf("\nSTAT; BEST_SOL; %f;\n", sim_inst->cost);

        if (wallEnd(sim_inst->wall_start) < sim_inst->timelimit)
            printf("STAT; WALL_TIME; %f;\n", wallEnd(sim_inst->wall_start));
        else
            printf("STAT; TIME_LIMIT_EXPIRED; %f;\n", sim_inst->timelimit);
    } else
    {
        printf("\nSTAT; BEST_SOL; %f;\n", tour_inst->cost);

        if (wallEnd(tour_inst->wall_start) < tour_inst->timelimit)
            printf("STAT; WALL_TIME; %f;\n", wallEnd(tour_inst->wall_start));
        else
            printf("STAT; TIME_LIMIT_EXPIRED; %f;\n", tour_inst->timelimit);
    }
}