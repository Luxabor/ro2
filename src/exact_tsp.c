/****************************************
*   UNIVERSITY OF PADOVA                *
*                                       *
*   Operational Research 2 Project      *
*   Travelling Salesman Problem         *
*                                       *
*   Luca Borin                          *
*   Student ID: 1134473                 *
*   Email: borin.luca.93@gmail.com      *
*                                       *
*   Iacopo Mandatelli                   *
*   Student ID: 1151791                  *
*   Email: mandamondo@gmail.com         *
*                                       *
*****************************************/

#include <exact_tsp.h>

void build_model(instance *inst, CPXENVptr env, CPXLPptr lp)
{
    if (inst->prob->nnodes <= 0)
        print_error("Can't create an empty model. Number of provided nodes <= 0.");

    double zero = 0.0; // one = 1.0;
    char binary = 'B';

    char **cname = (char **) calloc(1, sizeof(char *));
    cname[0] = (char *) calloc(100, sizeof(char));

    for (int i = 0; i < inst->prob->nnodes; i++)
    {
        for (int j = i + 1; j < inst->prob->nnodes; j++)
        {
            sprintf(cname[0], "x(%d,%d)", i + 1, j + 1);
            double obj = dist(inst, i, j);
            double ub = 1.0;
            if (CPXnewcols(env, lp, 1, &obj, &zero, &ub, &binary, cname)) print_error(" wrong CPXnewcols on x var.s");
            if (CPXgetnumcols(env, lp) - 1 != xpos(i, j, inst)) print_error(" wrong position for x var.s");
        }
    }

    for (int i = 0; i < inst->prob->nnodes; i++)  //degree of a node
    {
        int lastrow = CPXgetnumrows(env, lp);
        double rhs = 2.0;
        char sense = 'E';
        sprintf(cname[0], "degree(%d)", i + 1);
        if (CPXnewrows(env, lp, 1, &rhs, &sense, NULL, cname)) print_error(" wrong CPXnewrows [x1]");
        for (int j = 0; j < inst->prob->nnodes; j++)
            if (i != j && CPXchgcoef(env, lp, lastrow, xpos(i, j, inst), 1.0)) print_error(" wrong CPXchgcoef [x1]");
    }

    free(cname[0]);
    free(cname);
}

void build_model_heuristic(instance *inst, CPXENVptr env, CPXLPptr lp)
{
    //Build standard model
    build_model(inst, env, lp);
    initialize_instance(env, lp, inst);

    char bound_type = 'U';
    double ub = 0.0;
    int index = 0;

    //Set to zero all the upper bounds
    for (int i = 0; i < inst->prob->nnodes; ++i)
    {
        for (int j = i + 1; j < inst->prob->nnodes; ++j)
        {
            if (CPXchgbds(env, lp, 1, &index, &bound_type, &ub)) print_error(" wrong change coefficient");
            ++index;
        }
    }

    ub = 1.0;

    heur_shortest_edges_bound(inst);

    //Insert all the bounds
    for (int i = 0; i < inst->prob->nnodes; ++i) //for every vertex
    {
        for (int j = 0; j < inst->heur_num_shortest_edges; ++j) //for all the closest vertexes
        {
            index = xpos(i, inst->heur_shortest_edges[i][j].v2, inst);

            if (CPXchgbds(env, lp, 1, &index, &bound_type, &ub)) print_error(" wrong change coefficient");
        }
    }
}

void heuristic_reset_bounds(CPXENVptr env, CPXLPptr lp, instance *inst)
{
    char bound_type = 'U';
    double ub = 1.0;
    int index = 0;

    //Set to zero all the upper bounds
    for (int i = 0; i < inst->prob->nnodes; ++i)
    {
        for (int j = i + 1; j < inst->prob->nnodes; ++j)
        {
            if (CPXchgbds(env, lp, 1, &index, &bound_type, &ub)) print_error(" wrong change coefficient");
            index++;
        }
    }
}

int no_subtour(CPXENVptr env, CPXLPptr lp, instance *inst)
{
    //Build model
    build_model(inst, env, lp);

    initialize_instance(env, lp, inst);

    int status = CPXmipopt(env, lp);

    get_current_sol(env, lp, inst);

    if (inst->verbose > 3)
    {
        char plot_title[300];
        sprintf(plot_title, "TSP (%s) - Current cost: %3.0f", inst->solutionapproach, inst->best_val);
        gnuprint(inst->gnuplotPipe, plot_title, inst);
    }

    return status;
}

int simple_loop_method(CPXENVptr env, CPXLPptr lp, instance *inst)
{
    build_model(inst, env, lp);

    initialize_instance(env, lp, inst);

    return loop_method(env, lp, inst);
}


int weak_loop_method(CPXENVptr env, CPXLPptr lp, instance *inst)
{
    build_model(inst, env, lp);

    initialize_instance(env, lp, inst);

    return cycle_loop_method(env, lp, inst);
}


int lazy_constraint_method(CPXENVptr env, CPXLPptr lp, instance *inst, int initialize)
{
    if (initialize)
    {
        build_model(inst, env, lp);

        initialize_instance(env, lp, inst);

        inst->wall_start = wallStart();
    }

    install_callback(env, lp, inst);


    if ((inst->verbose > 3) && !strcmp(inst->printsolutiongap, "yes"))
    {
        fprintf(inst->gnuplotBoundPipe, "set term x11 0\n");
        fprintf(inst->gnuplotBoundPipe, "set title \"%s\"\n",
                "Gap reduction between the best integer and the best bound");
        fprintf(inst->gnuplotBoundPipe, "plot '-' with points pointtype 1 notitle\n");
    }


    int status = CPXmipopt(env, lp);

    get_current_sol(env, lp, inst);


    if ((inst->verbose > 3) && !strcmp(inst->printsolutiongap, "yes"))
    {
        fprintf(inst->gnuplotBoundPipe, "e\n");
    }

    uninstall_callback(env, inst);

    return 0;
}

int user_cut_method(CPXENVptr env, CPXLPptr lp, instance *inst)
{
    build_model(inst, env, lp);

    initialize_instance(env, lp, inst);

    install_callback(env, lp, inst);

    int status = CPXmipopt(env, lp);

    get_current_sol(env, lp, inst);

    uninstall_callback(env, inst);

    return status;
}

int heuristic_loop_method(CPXENVptr env, CPXLPptr lp, instance *inst)
{
    //Build the model using
    build_model_heuristic(inst, env, lp);

    CPXsetdblparam(env, CPX_PARAM_EPGAP, inst->opt_gap);

    struct timespec wall_start = wallStart();

    loop_method(env, lp, inst);

    if (time_limit_expired(inst) == 1)
    {
        printf("Time limit expired. Increase it in order to execute entirely the heuristic loop approach.\n");
        return 0;
    }

    if (inst->verbose > 3)
        printf("STAT; LOOP_HEURISTIC; %f;\n", wallEnd(wall_start));

    heuristic_reset_bounds(env, lp, inst);

    CPXsetdblparam(env, CPX_PARAM_EPGAP, 0.0);

    wall_start = wallStart();

    loop_method(env, lp, inst);

    if (inst->verbose > 3)
        printf("STAT; LOOP_AFTER_HEURISTIC; %f;\n", wallEnd(wall_start));

    return 0;
}

void printStats(instance *inst, double cpu_time, double wall_time)
{
    if (inst->verbose >= 3)
    {
        printf("Wall time elapsed for the resolution of the TSP problem: %f seconds\n", wall_time);
        printf("TSP problem solved in %f seconds\n", cpu_time);
        printf("STAT; WALL_TIME; %f;\n", wall_time);
        printf("STAT; CPU_TIME; %f;\n", cpu_time);
        printf("STAT; N_THREADS; %d;\n", inst->n_threads);
        printf("STATS; BEST_LOWER_BOUND; %lf\n", inst->best_lb);
        printf("STATS; BEST_SOLUTION; %lf\n", inst->best_val);

        if (strcmp(inst->solutionapproach, "usercut_callback") == 0)
        {
            for (int i = 0; i < inst->n_threads; ++i)
            {
                printf("STAT; USERCUT_CALLBACK_TIME-%d; %lf;\n", i, inst->time_usercut[i]);
            }

            for (int i = 0; i < inst->n_threads; ++i)
            {
                printf("STAT; LAZY_CALLBACK_TIME-%d; %lf;\n", i, inst->time_lazy[i]);
            }
        }

        if (strcmp(inst->solutionapproach, "lazy_callback") == 0)
        {
            for (int i = 0; i < inst->n_threads; ++i)
            {
                printf("STAT; LAZY_CALLBACK_TIME-%d; %lf;\n", i, inst->time_lazy[i]);
            }
        }

    } else
    {
        printf("TSP problem solved in %f seconds\n", cpu_time);
    }
}

void print_instance(instance *inst)
{
    printf("Stored problem instance.\n");
    printf("Input file: %s\n", inst->inputfile);
    printf("Nodes: %d\n", inst->prob->nnodes);
    printf("Time limit: %f\n", inst->timelimit);
    printf("Best lower bound: %f\n", inst->best_lb);
    printf("Best solution: %f\n", inst->best_val);


    if (inst->verbose >= 2)
    {
        printf("Node coordinates (x,y):\n");
        for (int i = 0; i < inst->prob->nnodes; ++i)      //print the coordinates of the points
        {
            printf("  - %d) %3.10f, %3.10f\n", i + 1, inst->prob->coord[i].x, inst->prob->coord[i].y);
        }
    }
}

int xpos(int i, int j, instance *inst)
{
    if (i == j) return -1;
    if (i > j) return xpos(j, i, inst);
    return i * inst->prob->nnodes + j - (i + 1) * (i + 2) / 2;
}

void free_instance(instance *inst)
{
    if (!inst->prob)
    {
        if (!inst->prob->coord)
            free(inst->prob->coord);

        inst->prob->coord = NULL;

        free(inst->prob);
    }

    if (!inst->heur_shortest_edges)
        free(inst->heur_shortest_edges);

    if (!inst->best_sol)
        free(inst->best_sol);

    if (!inst->connected_vertex)
        free(inst->connected_vertex);

    if (!inst->time_lazy)
        free(inst->time_lazy);

    if (!inst->time_usercut)
        free(inst->time_usercut);

    if (inst->gnuplotPipe != NULL)
        pclose(inst->gnuplotPipe);

    if (inst->gnuplotBoundPipe != NULL)
        pclose(inst->gnuplotBoundPipe);


    inst->prob = NULL;
    inst->heur_shortest_edges = NULL;
    inst->best_sol = NULL;
    inst->connected_vertex = NULL;
    inst->time_lazy = NULL;
    inst->time_usercut = NULL;
    inst->gnuplotPipe = NULL;
    inst->gnuplotBoundPipe = NULL;

    free(inst);
}

double dist(instance *inst, int point1, int point2)
{
    if (strcmp(inst->prob->weight_type, "EUC_2D") == 0)
    {
        return ((double) euclideanDistance(inst, point1, point2));
    }

    char error[300];

    sprintf(error, "Weight type '%s' not admissible", inst->prob->weight_type);

    print_error(error);

    return -1;
}

int euclideanDistance(instance *inst, int point1, int point2)
{
    if (point1 == point2)
    { return 0; }
    double first = pow((inst->prob->coord[point1].x - inst->prob->coord[point2].x), (double) 2);
    double second = pow((inst->prob->coord[point1].y - inst->prob->coord[point2].y), (double) 2);
    double ce = sqrt(first + second);
    return (round(ce));
}


int tspfile_to_gnuplot(instance *inst)
{
    FILE *f_in = fopen(inst->solutionfile, "r");
    FILE *f_out = fopen(inst->plotfile, "w");

    if (f_in == NULL)
    {
        printf("Error opening %s", inst->solutionfile);
        return -1;
    }

    char line[300];
    char *token;

    fgets(line, sizeof(line), f_in);
    token = strtok(line, " ");
    int p1, p2, value;

    while (strcmp("<variables>\n", token) != 0)
    {
        fgets(line, sizeof(line), f_in);
        token = strtok(line, " ");
    }

    fgets(line, sizeof(line), f_in);

    int code = 3;

    //Creates a file containing the edges used
    double x1, x2, y1, y2;

    while (code == 3)
    {
        code = sscanf(line, "%*s name=\"x(%d,%d)\" index=\"%*d\" value=\"%d\"/>", &p1, &p2, &value);

        if (code != 0 && value != 0)
        {
            x1 = inst->prob->coord[p1 - 1].x;
            x2 = inst->prob->coord[p2 - 1].x;
            y1 = inst->prob->coord[p1 - 1].y;
            y2 = inst->prob->coord[p2 - 1].y;

            fprintf(f_out, "%lf %lf\n%lf %lf\n\n", x1, y1, x2, y2);
        }

        fgets(line, sizeof(line), f_in);
    }

    fclose(f_in);
    fclose(f_out);

    return 0;
}

void kruskal(instance *inst)
{
    for (int i = 0; i < inst->prob->nnodes; i++)  //initialization
    {
        inst->connected_vertex[i] = i + 1;
    }

    for (int i = 0; i < inst->prob->nnodes; i++)      //populating connected_vertex
    {
        for (int j = i + 1; j < inst->prob->nnodes; j++)
        {
            if (inst->best_sol[xpos(i, j, inst)] > 0.5)               //if the edge is selected
            {
                if (inst->connected_vertex[i] < inst->connected_vertex[j])
                {
                    int tmp = inst->connected_vertex[j];
                    for (int z = 0; z < inst->prob->nnodes; z++)
                    {
                        if (inst->connected_vertex[z] == tmp)
                        {
                            inst->connected_vertex[z] = inst->connected_vertex[i];
                        }
                    }
                } else
                {
                    int tmp = inst->connected_vertex[i];
                    for (int z = 0; z < inst->prob->nnodes; z++)
                    {
                        if (inst->connected_vertex[z] == tmp)
                        {
                            inst->connected_vertex[z] = inst->connected_vertex[j];
                        }
                    }
                }
            }
        }
    }
}


int tspvect_to_gnuplot(instance *inst)
{
    FILE *f_out = fopen(inst->plotfile, "w");

    double x1, x2, y1, y2;

    for (int i = 0; i < inst->prob->nnodes; i++)
    {
        for (int j = i + 1; j < inst->prob->nnodes; j++)
        {
            if (inst->best_sol[xpos(i, j, inst)] > 0.5)
            {
                x1 = inst->prob->coord[i].x;
                x2 = inst->prob->coord[j].x;
                y1 = inst->prob->coord[i].y;
                y2 = inst->prob->coord[j].y;

                fprintf(f_out, "%lf %lf\n%lf %lf\n\n", x1, y1, x2, y2);
            }
        }
    }

    fclose(f_out);

    return 0;
}

void gnuBoundPrint(FILE *gp, instance *inst, double inte, double bound)
{
    double wall_time_elapsed = wallEnd(inst->wall_start);

    fprintf(gp, "%lf %lf\n", wall_time_elapsed, inte);

    fprintf(gp, "%lf %lf\n", wall_time_elapsed, bound);

    fflush(gp);
}

void gnuprint(FILE *gp, char *title, instance *inst)
{
    fprintf(gp, "set term x11 1\n");

    fprintf(gp, "clear\n");

    fprintf(gp, "set style line 1 lc rgb '#0060ad' lt 1 lw 2 pt 7 ps 1.5\n");

    fprintf(gp, "set title \"%s\"\n", title);

    fprintf(gp, "plot '-' with linespoints ls 1 notitle\n");

    //Attach also point label...confusing
    //fprintf(gp,"plot '-' using 1:2 with linespoints ls 1 notitle, '' using 1:2:3 with labels notitle\n");

    double x1, x2, y1, y2;

    for (int i = 0; i < inst->prob->nnodes; i++)
    {
        for (int j = i + 1; j < inst->prob->nnodes; j++)
        {
            if (inst->best_sol[xpos(i, j, inst)] > 0.5)
            {
                x1 = inst->prob->coord[i].x;
                x2 = inst->prob->coord[j].x;
                y1 = inst->prob->coord[i].y;
                y2 = inst->prob->coord[j].y;

                fprintf(gp, "%lf %lf\n%lf %lf\n\n", x1, y1, x2, y2);

                //Attach also point label...confusing
                //fprintf(gp, "%lf %lf %d\n%lf %lf %d\n\n", x1, y1, xpos(i, j, inst), x2, y2, xpos(i, j, inst));

                fflush(gp);
            }
        }
    }
    fprintf(gp, "e\n");

    fflush(gp);
}

int plot(instance *inst)
{
    char command[300];

    sprintf(command, "gnuplot -persist -e \"filename='%s'\" ../out/gnuplotScript.plt", inst->plotfile);

    if (system(command) == -1)
    {
        print_error(" error within the system call to gnuplot");
    }
    return 0;
}

int get_current_sol(CPXENVptr env, CPXLPptr lp, instance *inst)
{
    //If a filename is provided it saves the solution
    if (strcmp(inst->solutionfile, "") != 0 && inst->verbose > 3)
        CPXwritemipstarts(env, lp, inst->solutionfile, 0, 0);

    //Get the best lower bound available
    CPXgetbestobjval(env, lp, &inst->best_lb);

    //Get the best current path
    CPXgetx(env, lp, inst->best_sol, 0, inst->ncols - 1);

    //Get the best value
    CPXgetobjval(env, lp, &inst->best_val);

    //Return the qualification of the solution
    return CPXgetstat(env, lp);
}

int loop_method(CPXENVptr env, CPXLPptr lp, instance *inst)
{
    int status = CPXmipopt(env, lp);

    if (inst->verbose > 3)
    {
        char sol[300];
        solution_code(status, sol);
        printf("Current solution: %s\n", sol);
    }

    get_current_sol(env, lp, inst);

    kruskal(inst);

    if (inst->verbose > 3)
    {
        char plot_title[300];

        sprintf(plot_title, "TSP (%s) - Iteration: %d - Current cost: %3.0f", inst->solutionapproach,
                inst->loop_iterations,
                inst->best_val);
        gnuprint(inst->gnuplotPipe, plot_title, inst);
    }

    int cycles = 0;
    int id_size = inst->prob->nnodes / 3;
    int *id_cc = (int *) calloc(id_size, sizeof(int));
    int index = 0;

    //Find the distinct connected components
    for (int i = 0; i < inst->prob->nnodes; ++i)
    {
        if (find(inst->connected_vertex[i], id_cc, id_size) == -1)
            id_cc[index++] = inst->connected_vertex[i];
    }

    char **cname = (char **) calloc(1, sizeof(char *));
    cname[0] = (char *) calloc(100, sizeof(char));

    while (index > 1 && time_limit_expired(inst) != 1)
    {
        inst->loop_iterations++;

        //Find all connected components
        for (int i = 0; i < index; ++i) //for every connected component
        {
            int lastrow = CPXgetnumrows(env, lp);
            char sense = 'L';
            double rhs = 0.0;
            sprintf(cname[0], "cc%d%d", cycles, id_cc[i]);

            if (CPXnewrows(env, lp, 1, &rhs, &sense, NULL, cname)) print_error(" wrong CPXnewrows");

            for (int k = 0; k < inst->prob->nnodes; ++k)
                if ((inst->connected_vertex[k] == id_cc[i]))
                    ++rhs;

            for (int k = 0; k < inst->prob->nnodes; ++k) //for every vertex
            {
                if ((inst->connected_vertex[k] == id_cc[i]))
                {
                    for (int j = k + 1; j < inst->prob->nnodes; j++)
                    {

                        if ((inst->connected_vertex[j] == id_cc[i]))
                        {
                            if (CPXchgcoef(env, lp, lastrow, xpos(k, j, inst), 1.0))
                                print_error(" wrong change coefficient");
                        }

                    }
                }
            }

            //Change RHS
            if (CPXchgcoef(env, lp, lastrow, -1, rhs - 1)) print_error(" wrong change coefficient");
        }

        status = CPXmipopt(env, lp);

        if (status != 0)
        {
            char error[300];
            sprintf(error, "CPLEX raised an error during the optimization process (code %d)", status);
            print_error(error);
        }

        status = CPXgetstat(env, lp);

        if (status != CPXMIP_OPTIMAL)
        {
            char exp[300];
            solution_code(status, exp);
            printf("Couldn't find a proper heuristic solution (%s).\n"
                           "Moving to standard loop method.\n", exp);
            break;
        }

        if (inst->verbose > 3)
        {
            char sol[300];
            solution_code(status, sol);
            printf("Current solution: %s\n", sol);
        }

        get_current_sol(env, lp, inst);

        kruskal(inst);


        if (inst->verbose > 3)
        {
            char plot_title[300];

            sprintf(plot_title, "TSP (%s) - Iteration: %d - Current cost: %3.0f", inst->solutionapproach,
                    inst->loop_iterations,
                    inst->best_val);
            gnuprint(inst->gnuplotPipe, plot_title, inst);
        }

        index = 0;

        for (int i = 0; i < id_size; ++i)
        {
            id_cc[i] = 0;
        }

        //Find the distinct connected components
        for (int i = 0; i < inst->prob->nnodes; ++i)
        {
            if (find(inst->connected_vertex[i], id_cc, id_size) == -1)
                id_cc[index++] = inst->connected_vertex[i];
        }

        cycles++;
    }

    free(id_cc);
    free(cname[0]);
    free(cname);

    return 0;
}


int cycle_loop_method(CPXENVptr env, CPXLPptr lp, instance *inst)
{
    int status = CPXmipopt(env, lp);

    if (inst->verbose > 3)
    {
        char sol[300];

        solution_code(status, sol);

        printf("Current solution: %s\n", sol);
    }

    get_current_sol(env, lp, inst);

    kruskal(inst);

    char plot_title[300];

    sprintf(plot_title, "TSP (%s) - Iteration: %d - Current cost: %3.0f", inst->solutionapproach, inst->loop_iterations,
            inst->best_val);

    if (inst->verbose > 3)
        gnuprint(inst->gnuplotPipe, plot_title, inst);

    int cycles = 0;
    int id_size = inst->prob->nnodes / 3;
    int *id_cc = (int *) calloc(id_size, sizeof(int));
    int index = 0;

    //Find the distinct connected components
    for (int i = 0; i < inst->prob->nnodes; ++i)
    {
        if (find(inst->connected_vertex[i], id_cc, id_size) == -1)
            id_cc[index++] = inst->connected_vertex[i];
    }

    char **cname = (char **) calloc(1, sizeof(char *));
    cname[0] = (char *) calloc(100, sizeof(char));

    while (index > 1 && time_limit_expired(inst) != 1)
    {
        inst->loop_iterations++;

        //Find all connected components
        for (int i = 0; i < index; ++i) //for every connected component
        {
            int lastrow = CPXgetnumrows(env, lp);
            char sense = 'L';
            double rhs = 0.0;
            sprintf(cname[0], "cc%d%d", cycles, id_cc[i]);

            if (CPXnewrows(env, lp, 1, &rhs, &sense, NULL, cname)) print_error(" wrong CPXnewrows");

            for (int k = 0; k < inst->prob->nnodes; ++k)
                if ((inst->connected_vertex[k] == id_cc[i]))
                    ++rhs;

            for (int k = 0; k < inst->prob->nnodes; ++k) //for every vertex
            {
                if ((inst->connected_vertex[k] == id_cc[i]))
                {
                    for (int j = k + 1; j < inst->prob->nnodes; j++)
                    {

                        if ((inst->best_sol[xpos(k, j, inst)] > 0.5) && (inst->connected_vertex[j] == id_cc[i]))
                        {
                            if (CPXchgcoef(env, lp, lastrow, xpos(k, j, inst), 1.0))
                                print_error(" wrong change coefficient");
                        }

                    }
                }
            }

            //Change RHS
            if (CPXchgcoef(env, lp, lastrow, -1, rhs - 1)) print_error(" wrong change coefficient");
        }

        status = CPXmipopt(env, lp);

        if (inst->verbose > 3)
        {
            char sol[300];

            solution_code(status, sol);

            printf("Current solution: %s\n", sol);
        }

        get_current_sol(env, lp, inst);

        kruskal(inst);

        sprintf(plot_title, "TSP (%s) - Iteration: %d - Current cost: %3.0f", inst->solutionapproach,
                inst->loop_iterations, inst->best_val);

        if (inst->verbose > 3)
            gnuprint(inst->gnuplotPipe, plot_title, inst);

        index = 0;

        for (int i = 0; i < id_size; ++i)
        {
            id_cc[i] = 0;
        }

        //Find the distinct connected components
        for (int i = 0; i < inst->prob->nnodes; ++i)
        {
            if (find(inst->connected_vertex[i], id_cc, id_size) == -1)
                id_cc[index++] = inst->connected_vertex[i];
        }

        cycles++;
    }

    free(id_cc);
    free(cname[0]);
    free(cname);

    return status;
}

int find(int num, int *array, int size)
{
    for (int i = 0; i < size; ++i)
    {
        if (array[i] == num)
            return i;
    }
    return -1;
}

void heur_shortest_edges_bound(instance *inst)
{
    for (int i = 0; i < inst->prob->nnodes; i++)      //iterate on the nodes
    {
        double maxDist = 9e+09;                    //dist of the last element
        int counter = 0;
        for (int j = 0; j < inst->prob->nnodes; j++)
        {
            if (i != j)             //avoid self-loop
            {
                double dist_i_j = dist(inst, i, j);

                if (counter < inst->heur_num_shortest_edges)
                {
                    counter++;
                    edge edge_i_j = {i, j, dist_i_j};
                    maxDist = sorted_insert(edge_i_j, inst->heur_shortest_edges[i], inst->heur_num_shortest_edges);
                } else
                {
                    if (dist_i_j < maxDist)
                    {
                        edge edge_i_j = {i, j, dist_i_j};
                        maxDist = sorted_insert(edge_i_j, inst->heur_shortest_edges[i], inst->heur_num_shortest_edges);
                    }
                }
            }
        }
    }
}

double sorted_insert(edge input, edge *list, int size)
{
    double maxDist = -1;
    int maxIndex = 0;
    bool empty = false;
    for (int i = 0; i < size; ++i)
    {
        if (list[i].length == 0 && !empty)
        {
            empty = true;
            maxIndex = i;
        }
    }
    if (empty)
    {
        list[maxIndex] = input;
    } else
    {
        for (int i = 0; i < size; ++i)      //trovo l'elemento da rimuovere, quello con distanza massima
        {
            if (list[i].length > maxDist)
            {
                maxDist = list[i].length;
                maxIndex = i;
            }
        }
        list[maxIndex] = input;
    }

    maxDist = -1;
    for (int i = 0; i < size; ++i)      //trovo la distanza massima da tornare
    {
        if (list[i].length > maxDist)
        {
            maxDist = list[i].length;
        }
    }
    return maxDist;
}

void install_callback(CPXENVptr env, CPXLPptr lp, instance *inst)
{
    if (inst->verbose >= 30)
        printf("\nInstalling callbacks...\n");

    //Manage preprocessing conversion
    CPXsetintparam(env, CPX_PARAM_MIPCBREDLP, CPX_OFF);

    //Perform only linear reductions in order to admit user's cuts
    CPXsetintparam(env, CPX_PARAM_PRELINEAR, 0);

    //Add callback
    CPXsetlazyconstraintcallbackfunc(env, lazy_tsp_callback, inst);

    //Add informative callback
    if (!strcmp(inst->solutionapproach, "lazy_callback") && !strcmp(inst->printsolutiongap, "yes"))
        CPXsetincumbentcallbackfunc(env, info_tsp_callback, inst);
    //CPXsetnodecallbackfunc(env, info_tsp_callback, inst);

    //Add user cut callback
    if (!strcmp(inst->solutionapproach, "usercut_callback"))
    {
        CPXsetusercutcallbackfunc(env, cut_tsp_callback, inst);
    }

    //Perform only primal reductions
    CPXsetintparam(env, CPX_PARAM_REDUCE, CPX_PREREDUCE_PRIMALONLY);

    CPXsetintparam(env, CPX_PARAM_THREADS, inst->n_threads);
}

static int CPXPUBLIC
info_tsp_callback(CPXCENVptr env, void *cbdata, int wherefrom, void *cbhandle, double objval, double *x, int *isfeas_p,
                  int *useraction_p)
{
    if (wherefrom == CPX_CALLBACK_MIP_INCUMBENT_MIPSTART) return 0;

    //Get a proper instance pointer
    instance *inst = (instance *) cbhandle;

    /* Initialize useraction to indicate no user node selection */
    *useraction_p = CPX_CALLBACK_DEFAULT;

    int status = 0;
    double best_integer;
    double best_bound;

    status = CPXgetcallbackinfo(env, cbdata, wherefrom, CPX_CALLBACK_INFO_BEST_INTEGER, &best_integer);
    status = CPXgetcallbackinfo(env, cbdata, wherefrom, CPX_CALLBACK_INFO_BEST_REMAINING, &best_bound);

    if ((best_integer < CPX_INFBOUND) && (best_bound < CPX_INFBOUND))
    {
        if (inst->verbose > 3)
            gnuBoundPrint(inst->gnuplotBoundPipe, inst, best_integer, best_bound);
        printf("STAT; COUNTER; BEST_INTEGER; BEST_BOUND; %f; %f; %f;\n", wallEnd(inst->wall_start), best_integer,
               best_bound);
        //togliere il commento sopra per la run su blade
    }

    return status;
}

static int CPXPUBLIC lazy_tsp_callback(CPXCENVptr env, void *cbdata, int wherefrom, void *cbhandle, int *useraction_p)
{
    struct timespec wall_start = wallStart();      //instant of start for the wall clock

    //Get a proper instance pointer
    instance *inst = (instance *) cbhandle;

    if (time_limit_expired(inst) == 1)
        return 1;

    //Set standard output
    *useraction_p = CPX_CALLBACK_DEFAULT;

    //Create a local instance
    instance *local_inst = create_instance();

    create_callback_instance(inst, local_inst);

    //Create a local copy of the current solution
    if (CPXgetcallbacknodex(env, cbdata, wherefrom, local_inst->best_sol, 0, local_inst->ncols - 1))
        print_error("Error asking inside the callback for the current problem solution");

    //Find the connected components in the current solution
    kruskal(local_inst);

    int id_size = local_inst->prob->nnodes / 3;
    int *id_cc = (int *) calloc(id_size, sizeof(int)); //Keep the id of the connected components
    int *id_cc_edges = (int *) calloc(id_size,
                                      sizeof(int)); //Keep how many vertexes belong to that connected component

    int *id_cc_vertexes = (int *) calloc(id_size,
                                         sizeof(int));

    int index = 0;
    int pos = 0;

    //Find the distinct connected components and count the number of vertexes that belong to each one
    for (int i = 0; i < local_inst->prob->nnodes; ++i)
    {
        fflush(stdout);
        pos = find(local_inst->connected_vertex[i], id_cc, id_size);

        if (pos == -1) //new id found
        {
            ++id_cc_vertexes[index];
            id_cc[index] = local_inst->connected_vertex[i];

            ++index;
        } else //old id found
        {
            ++id_cc_vertexes[pos];
        }
    }

    for (int i = 0; i < index; ++i)
        id_cc_edges[i] = comb(id_cc_vertexes[i], 2);

    if (index > 1) //if there are more than one connected component, a cut should be generated
    {
        int vec_pos = 0;
        int purgeable = CPX_USECUT_FORCE;

        for (int i = 0; i < index; ++i) //for every connected component
        {
            double *cutval = (double *) calloc(id_cc_edges[i], sizeof(double));
            int *cutind = (int *) calloc(id_cc_edges[i], sizeof(int));

            int counter = 0;

            for (int k = 0; k < local_inst->prob->nnodes; ++k)
            {
                if ((local_inst->connected_vertex[k] == id_cc[i]))
                {
                    for (int j = k + 1; j < local_inst->prob->nnodes; j++)
                    {
                        if ((local_inst->connected_vertex[j] == id_cc[i]))
                        {
                            vec_pos = xpos(k, j, local_inst);
                            cutval[counter] = 1.0;
                            cutind[counter] = vec_pos;
                            counter++;
                        }
                    }
                }
            }

            if (CPXcutcallbackadd(env, cbdata, wherefrom, id_cc_edges[i], (double) (id_cc_vertexes[i] - 1), 'L',
                                  cutind, cutval,
                                  purgeable))
                print_error("Error in the cut generation.");

            free(cutval);
            free(cutind);
        }

        //Signal that the current solution is not admissible
        *useraction_p = CPX_CALLBACK_SET;
    }

    free(id_cc);
    free(id_cc_edges);
    free_instance(local_inst);


    //Add the callback time
    int num_thread;
    CPXgetcallbackinfo(env, cbdata, wherefrom, CPX_CALLBACK_INFO_MY_THREAD_NUM, &num_thread);
    inst->time_lazy[num_thread] += wallEnd(wall_start);

    return 0;
}

static int CPXPUBLIC cut_tsp_callback(CPXCENVptr env, void *cbdata, int wherefrom, void *cbhandle, int *useraction_p)
{
    //Get a proper instance pointer
    instance *inst = (instance *) cbhandle;

    if (time_limit_expired(inst) == 1)
        return 1;

    //Set standard output
    *useraction_p = CPX_CALLBACK_DEFAULT;

    //Give priority to the cuts generated by Cplex
    if (wherefrom != CPX_CALLBACK_MIP_CUT_LAST)
        return 0;

    struct timespec wall_start = wallStart();      //instant of start for the wall clock

    int num_thread;
    CPXgetcallbackinfo(env, cbdata, wherefrom, CPX_CALLBACK_INFO_MY_THREAD_NUM, &num_thread);

    //In order to avoid an eccessive amount of cuts added to the model, "randomize" the addition
    int node_count;
    CPXgetcallbackinfo(env, cbdata, wherefrom, CPX_CALLBACK_INFO_NODE_COUNT, &node_count);


    if (node_count % 10 != 0 || node_count == 0)
        return 0;


    //Create a local instance
    instance *local_inst = create_instance();

    create_callback_instance(inst, local_inst);

    //Create a local copy of the current solution
    if (CPXgetcallbacknodex(env, cbdata, wherefrom, local_inst->best_sol, 0, local_inst->ncols - 1))
        print_error("Error asking inside the callback for the current problem solution");


    /*
    CONCORDE DOCUMENTATION: CCcut_connect_components
    File:
        CUT/connect.c
    Header:
        cut.h
    Prototype:
        int CCcut_connect_components (int ncount, int ecount, int *elist,
        double *x, int *ncomp, int **compscount, int **comps)
    Description:
    RETURNS the connected components of the graph given by the edgeset
        - ncount is the number of nodes
        - ecount is the number of edges
        - elist is the edge list in node node format
        - x is an vector of length ecount (it can be NULL); is it is not
        NULL, then the connected components will be for the graph
        consisting of the positive edges
        - ncomp will return the number of connected components
        - compscount will return the number of nodes in each of the
        components (it will be an ncomp long array)
        - comps will return the nodes in the components (it will be an
        ncount array, with the first compscount[0] elements making up
        the first component, etc.)
     */


    //Create the list of edges (node-node)
    int *elist = (int *) malloc(2 * inst->ncols * sizeof(int));
    int edge_counter = 0;

    for (int i = 0; i < inst->prob->nnodes; ++i)
    {
        for (int j = i + 1; j < inst->prob->nnodes; ++j)
        {
            elist[edge_counter++] = i;
            elist[edge_counter++] = j;
        }
    }

    int ncomp = 0;
    int *compscount;
    int *comps;

    if (CCcut_connect_components(inst->prob->nnodes, inst->ncols, elist, local_inst->best_sol, &ncomp, &compscount,
                                 &comps))
    {
        print_error("Error during the execution of the CONCORDE CCcut_connect_components function.\n");
    }

    if (ncomp > 1) //if there are more than one connected component, a cut should be generated
    {
        int vec_pos = 0;
        int purgeable = CPX_USECUT_FORCE;

        for (int i = 0; i < ncomp; ++i) //for every connected component
        {
            int nedges_cc = comb(compscount[i], 2);
            double *cutval = (double *) calloc(nedges_cc, sizeof(double));
            int *cutind = (int *) calloc(nedges_cc, sizeof(int));

            int counter = 0;

            //For every node pair in the connected component set the proper value
            for (int j = 0; j < compscount[i]; ++j)
            {
                for (int k = j + 1; k < compscount[i]; ++k)
                {
                    vec_pos = xpos(comps[k], comps[j], local_inst);
                    cutval[counter] = 1.0;
                    cutind[counter] = vec_pos;
                    counter++;
                }
            }


            if (CPXcutcallbackadd(env, cbdata, wherefrom, nedges_cc, (double) (nedges_cc - 1), 'L',
                                  cutind, cutval,
                                  purgeable))
                print_error("Error in the cut generation.");

            free(cutval);
            free(cutind);
        }

        *useraction_p = CPX_CALLBACK_SET;
    } else if (ncomp == 1)
    {

        /*
        CONCORDE DOCUMENTATION: CCcut_violated_cuts
        File:
            CUT/mincut.c
        Header:
            cut.h
        Prototype:
            int CCcut_violated_cuts (int ncount, int ecount, int *elist,
            double *dlen, double cutoff, int (*doit_fn) (double, int,
            int *, void *), void *pass_param)
        Description:
            COMPUTES the global minimum cut, but calls the doit_fn function
            for any cut the algorithm encounters that has capacity at most
            cutoff.
                - doit_fn (if not NULL) will be called for each cut having capacity
                less than or equal to the cutoff value; the arguments will be the
                value of the cut, the number of nodes in the cut, the array of
                the members of the cut, and pass_param.
                - pass_param will be passed to doit_fn; it can be NULL or it can be
                used to pass information to the doit_fn function.
            NOTES: This function assumes graph is connected.
            NOTES: This code works with undirected graphs. The shrinking routines
            assume that we are working with the TSP and not interested in cuts
            of weight 2.0 or more.
         */

        //Initialize the structure
        input_concorde *input_data = (input_concorde *) malloc(sizeof(input_concorde));
        input_data->cbdata = cbdata;
        input_data->env = env;
        input_data->inst = local_inst;
        input_data->wherefrom = wherefrom;
        input_data->useraction_p = useraction_p;

        //Call the Concorde function
        if (CCcut_violated_cuts(inst->prob->nnodes, inst->ncols, elist, local_inst->best_sol, 2.0 - EPSILON,
                                add_flow_cut,
                                (void *) input_data))
        {
            print_error("Error during CCcut_violated_cuts\n");
        }

        free(input_data);
    }

    free(elist);
    free(compscount);
    free(comps);

    free_instance(local_inst);

    //Add the callback time
    inst->time_usercut[num_thread] += wallEnd(wall_start);

    return 0;
}


int add_flow_cut(double cutval, int cutcount, int *cut, void *input_data)
{
    input_concorde *input = (input_concorde *) input_data;

    int vec_pos = 0;
    int purgeable = CPX_USECUT_FORCE;

    int nedges_cc = comb(cutcount, 2);
    double *cutvalues = (double *) calloc(nedges_cc, sizeof(double));
    int *cutind = (int *) calloc(nedges_cc, sizeof(int));

    int counter = 0;

    //For every node pair in the connected component set the proper value
    for (int j = 0; j < cutcount; ++j)
    {
        for (int k = j + 1; k < cutcount; ++k)
        {
            vec_pos = xpos(cut[k], cut[j], input->inst);
            cutvalues[counter] = 1.0;
            cutind[counter] = vec_pos;
            counter++;
        }
    }


    if (CPXcutcallbackadd(input->env, input->cbdata, input->wherefrom, nedges_cc, 2.0, 'G',
                          cutind, cutvalues, purgeable))
        print_error("Error in the cut generation.");

    free(cutvalues);
    free(cutind);


    *(input->useraction_p) = CPX_CALLBACK_SET;

    return 0;
}


void create_callback_instance(instance *source, instance *dest)
{
    dest->connected_vertex = (int *) calloc(source->prob->nnodes, sizeof(int));
    dest->best_sol = (double *) calloc(source->ncols, sizeof(double));
    dest->prob = (problem *) malloc(sizeof(problem));
    dest->ncols = source->ncols;
    dest->prob->nnodes = source->prob->nnodes;
    strcpy(dest->solutionapproach, "callback_internal");


    //Initialization useful only for plotting purposes
    if (source->verbose > 3)
    {
        dest->prob->coord = (point *) calloc(source->prob->nnodes, sizeof(point));
        memcpy(dest->prob->coord, source->prob->coord, source->prob->nnodes * sizeof(point));
    }
}

int uninstall_callback(CPXENVptr env, instance *inst)
{
    if (!strcmp(inst->solutionapproach, "lazy_callback"))
        return CPXsetlazyconstraintcallbackfunc(env, NULL, NULL);
    else if (!strcmp(inst->solutionapproach, "usercut_callback"))
        return CPXsetusercutcallbackfunc(env, NULL, NULL);
    else
        return 1;
}

void initialize_instance(CPXENVptr env, CPXLPptr lp, instance *inst)
{
    inst->ncols = CPXgetnumcols(env, lp);
    inst->best_sol = (double *) calloc(inst->ncols, sizeof(double));
    CPXgetnumcores(env, &inst->n_threads);
    inst->wall_start = wallStart();

    if (inst->verbose > 3)
        inst->gnuplotPipe = popen("gnuplot -persistent", "w");

    if (!strcmp(inst->solutionapproach, "callback_internal"))
    {
        inst->connected_vertex = (int *) malloc(inst->prob->nnodes * sizeof(int));
        inst->loop_iterations = 0;
        inst->time_usercut = (double *) calloc(inst->n_threads, sizeof(double));
        inst->time_lazy = (double *) calloc(inst->n_threads, sizeof(double));
    } else if (!strcmp(inst->solutionapproach, "loop_method")
               || !strcmp(inst->solutionapproach, "weak_loop_method"))
    {
        inst->connected_vertex = (int *) malloc(inst->prob->nnodes * sizeof(int));
        inst->loop_iterations = 0;
    } else if (!strcmp(inst->solutionapproach, "hard_fixing")
               || !strcmp(inst->solutionapproach, "local_branching")
               || !strcmp(inst->solutionapproach, "lazy_callback"))
    {
        inst->time_lazy = (double *) calloc(inst->n_threads, sizeof(double));
    } else if (!strcmp(inst->solutionapproach, "usercut_callback"))
    {
        inst->time_usercut = (double *) calloc(inst->n_threads, sizeof(double));
        inst->time_lazy = (double *) calloc(inst->n_threads, sizeof(double));
    } else if (!strcmp(inst->solutionapproach, "heuristic_loop_method"))
    {
        inst->connected_vertex = (int *) malloc(inst->prob->nnodes * sizeof(int));
        inst->loop_iterations = 0;

        //Allocate the structure
        inst->heur_shortest_edges = calloc(inst->prob->nnodes, inst->prob->nnodes * sizeof(edge *));
        for (int i = 0; i < inst->prob->nnodes; ++i)
            inst->heur_shortest_edges[i] = calloc(inst->heur_num_shortest_edges,
                                                  inst->heur_num_shortest_edges * sizeof(edge));
    }
}

int time_limit_expired(instance *inst)
{
    double tspan = wallEnd(inst->wall_start);
    if (tspan > inst->timelimit)
    {
        if (inst->verbose >= 1)
            printf("STATS; TIME_LIMIT_EXPIRED; %lf\n", tspan);
        return 1;
    }
    return 0;
}