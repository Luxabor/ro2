/****************************************
*   UNIVERSITY OF PADOVA                *
*                                       *
*   Operational Research 2 Project      *
*   Travelling Salesman Problem         *
*                                       *
*   Luca Borin                          *
*   Student ID: 1134473                 *
*   Email: borin.luca.93@gmail.com      *
*                                       *
*   Iacopo Mandatelli                   *
*   Student ID: 1151791                  *
*   Email: mandamondo@gmail.com         *
*                                       *
*****************************************/

#include "matheuristic_tsp.h"

int local_branching(CPXENVptr env, CPXLPptr lp, tour *inst_tour, instance *inst)
{
    printf("[LOC_BRANCH]: Finished heuristic, got a feasible solution.\n");

    int *sorted_tour = (int *) malloc(inst_tour->instance_problem->nnodes * sizeof(int));

    if (!strcmp(inst->heuristic_approach, "simulated_annealing"))
    {
        create_sorted_tour_annealing(inst_tour, sorted_tour);
    } else
    {
        create_sorted_tour(inst_tour, sorted_tour);
        create_ordered_tour(sorted_tour, inst_tour->start_node, inst_tour->instance_problem->nnodes);
    }

    //Set local timelimit
    CPXsetdblparam(env, CPX_PARAM_TILIM, inst->timelimit);

    int status;
    int nnodes = inst->prob->nnodes;
    int index;

    //MIP-START #######################################################################################################
    //Warm start MIP
    //https://www.ibm.com/support/knowledgecenter/SSSA5P_12.7.0/ilog.odms.cplex.help/refcallablelibrary/mipapi/addmipstarts.html
    int beg[1] = {0};
    int effortlevel[1] = {CPX_MIPSTART_CHECKFEAS}; //Check the feasibility of the warm start solution
    double *values = (double *) calloc(inst->ncols, sizeof(double)); //initialized to zero

    //Initialize varindices
    int *varindices = (int *) malloc(inst->ncols * sizeof(int));
    for (int i = 0; i < inst->ncols; ++i)
        varindices[i] = i;

    for (int i = 0; i < nnodes; ++i)
    {
        index = xpos(sorted_tour[(i % nnodes)], sorted_tour[((i + 1) % nnodes)], inst);
        values[index] = 1.0;
        //printf("\n Lato %d - %d \n", sorted_tour[(i % nnodes)], sorted_tour[((i + 1) % nnodes)]);
        //printf("Index of values[i] is %d \n", index);
        //printf("Sorted_tour[i]: %d, (%f - %f) \n", sorted_tour[i], inst_tour->instance_problem->coord[sorted_tour[i]].x, inst_tour->instance_problem->coord[sorted_tour[i]].y);
    }

    if (CPXaddmipstarts(env, lp, 1, inst->ncols, beg, varindices, values, effortlevel, NULL))
        print_error("Mip start error");
    //END MIP-STart####################################################################################################

    strcpy(inst->solutionapproach, "lazy_callback");
    inst->wall_start = wallStart();
    //inst->gnuplotPipe = popen("gnuplot -persistent", "w");

    double best_val = HUGE_VAL;
    char sol[300];
    char plot_title[300];
    int sol_code = 0;       //inizialmente non esiste soluzione
    int r[] = {3, 5, 10};
    int current_r = r[0];

    char **cname = (char **) calloc(1, sizeof(char *));
    cname[0] = (char *) calloc(100, sizeof(char));
    sprintf(cname[0], "local_branching_constraint");

    char sense = 'G';

    while ((wallEnd(inst->wall_start) < inst->timelimit))
    {
        //Add the constraints
        int lastrow = CPXgetnumrows(env, lp);
        double rhs = nnodes - current_r;
        if (CPXnewrows(env, lp, 1, &rhs, &sense, NULL, cname))
            print_error(" [LOC_BRANCH]: wrong CPXnewrows");

        for (int i = 0; i < nnodes; ++i)
        {
            index = xpos(sorted_tour[(i % nnodes)], sorted_tour[((i + 1) % nnodes)], inst);
            if (CPXchgcoef(env, lp, lastrow, index, 1.0))
                print_error(" [LOC_BRANCH]: wrong CPXchgcoef");
        }

        //Start the MIP solver
        lazy_constraint_method(env, lp, inst, 0);

        //solution_code(status, sol);
        //printf("Current solution: %s\n", sol);


        int k = 0;
        point *new_tour = (point *) malloc(nnodes * sizeof(point));
        for (int i = 0; i < nnodes; i++)
        {
            for (int j = i + 1; j < nnodes; j++)
            {
                if (inst->best_sol[xpos(i, j, inst)] > 0.5)
                {
                    new_tour[k].x = i;
                    new_tour[k].y = j;
                    k++;
                }
            }
        }

        order_solution(new_tour, sorted_tour, nnodes);

        //printf("\n\nBEST SOL with r = %d is %f \n\n", current_r, inst->best_val);

        //Aumento il raggio solo se non riesco a trovare soluzioni migliori
        if (inst->best_val < best_val)
        {
            best_val = inst->best_val;
        } else
        {
            if (current_r == r[0])
                current_r = r[1];
            else if (current_r == r[1])
                current_r = r[2];
        }

        if (CPXdelrows(env, lp, CPXgetnumrows(env, lp) - 1, CPXgetnumrows(env, lp) - 1))
            print_error("An error occurred removing the old constraints.");


    }

    free(cname[0]);
    free(cname);
    free(sorted_tour);

    //Stampo il miglior incumbent trovato
    if (inst->verbose > 3)
    {
        sprintf(plot_title, "TSP (local_branching) - Current cost: %3.0f", inst->best_val);
        gnuprint(inst->gnuplotPipe, plot_title, inst);
    }

    printf("[LOC_BRANCH]: finished");
    return 0;
}

void order_solution(point *tour, int *sorted_tour, int nnodes)
{
    int iteration = 0;
    int index = 0;
    sorted_tour[iteration] = (int) tour[iteration].x;
    int current = (int) tour[index].y;
    iteration++;
    while (iteration != nnodes - 1)
    {
        sorted_tour[iteration] = current;
        int next;
        bool found = false;
        for (int i = 0; i < nnodes; i++)
        {
            if ((tour[i].x == current) && (i != index) && (!found))
            {
                next = (int) tour[i].y;
                index = i;
                found = true;
            }
        }
        if (!found)     //non trovo current nelle x, cerco tra le y
        {
            for (int i = 0; i < nnodes; i++)
            {
                if ((tour[i].y == current) && (i != index) && (!found))
                {
                    next = (int) tour[i].x;
                    index = i;
                    found = true;
                }
            }
        }
        current = next;
        iteration++;
    }
    sorted_tour[iteration] = current;

    return;
}

void polish(CPXENVptr env, CPXLPptr lp, instance *inst, int *sorted_tour, int *fixed_edge, int nnodes)
{
    double zero = 0.0;
    char upper_and_lower_bound = 'B';

    int first_free = -1;
    int start_free = -1;
    int next = 0;
    int index;

    for (int i = 0; i < nnodes; ++i)
    {
        if (fixed_edge[next] == 0)
        {
            //If it's the first removed edge
            if (first_free == -1)
            {
                first_free = next;
            } else
            {
                //If the nodes are not the same and the distance is greater than one edge
                if (start_free != next && sorted_tour[start_free] != next)
                {
                    index = xpos(start_free, next, inst);

                    if (CPXchgbds(env, lp, 1, &index, &upper_and_lower_bound, &zero))
                    {
                        print_error("Polish function error.");
                    }
                }
            }

            start_free = sorted_tour[next];
        }

        next = sorted_tour[next];
    }


    if (fixed_edge[start_free] != 0)
    {
        index = xpos(first_free, start_free, inst);

        if (CPXchgbds(env, lp, 1, &index, &upper_and_lower_bound, &zero))
        {
            print_error("Polish function error.");
        }
    }
}


int hard_fixing(CPXENVptr env, CPXLPptr lp, tour *tour_inst, instance *inst)
{
    inst->wall_start = wallStart();      //instant of start for the wall clock

    float prob = 0.8;
    int prob_trials = 0;
    int max_prob_trials = 20;

    int nnodes = tour_inst->instance_problem->nnodes;
    int *sorted_tour = (int *) malloc(nnodes * sizeof(int));
    int *fixed_edge = (int *) calloc(nnodes, sizeof(int));

    double one = 1.0;
    char upper_and_lower_bound = 'B';

    int index;

    double best_val = HUGE_VAL;
    double *best_sol;

    strcpy(inst->solutionapproach, "lazy_callback");

    //Set local timelimit
    CPXsetdblparam(env, CPX_PARAM_TILIM, inst->timelimit);

    //Warm start MIP
    if (!strcmp(inst->heuristic_approach, "simulated_annealing"))
        create_sorted_tour_annealing(tour_inst, sorted_tour);
    else
        create_sorted_tour(tour_inst, sorted_tour);

    //(REF: https://www.ibm.com/support/knowledgecenter/SSSA5P_12.7.0/ilog.odms.cplex.help/refcallablelibrary/mipapi/addmipstarts.html)
    int beg[1] = {0};
    int effortlevel[1] = {CPX_MIPSTART_CHECKFEAS}; //Check the feasibility of the warm start solution
    double *values = (double *) calloc(inst->ncols, sizeof(double)); //initialized to zero

    //Initialize varindices
    int *varindices = (int *) malloc(inst->ncols * sizeof(int));
    for (int i = 0; i < inst->ncols; ++i)
        varindices[i] = i;

    for (int i = 0; i < nnodes; ++i)
    {
        index = xpos(i, sorted_tour[i], inst);

        values[index] = 1.0;
    }

    //Add the heuristic solution to the possible starting points
    if (CPXaddmipstarts(env, lp, 1, inst->ncols, beg, varindices, values, effortlevel, NULL))
        print_error("Wrong MIP start.");

    free(values);
    free(varindices);

    while (wallEnd(inst->wall_start) < inst->timelimit && prob >= 0)
    {
        //Create a sorted representation of the tour
        create_sorted_tour(tour_inst, sorted_tour);

        for (int i = 0; i < nnodes; ++i)
        {
            //Fix a tour variable to 1 with probability prob
            if ((double) rand() / (double) RAND_MAX < prob)
            {
                index = xpos(i, sorted_tour[i], inst);

                CPXchgbds(env, lp, 1, &index, &upper_and_lower_bound, &one);
                fixed_edge[i] = 1;
            }
        }

        //Remove the edges that certainly would create separate connected components
        polish(env, lp, inst, sorted_tour, fixed_edge, nnodes);

        //Start MIP solver
        lazy_constraint_method(env, lp, inst, 0);

        if (best_val <= inst->best_val) //No improvements
        {
            prob_trials++;

            if (prob_trials > max_prob_trials)
            {
                prob -= 0.2;
                prob_trials = 0;

                if (inst->verbose > 3)
                    printf("Reduced probability to %lf.\n", prob);
            }
        } else
        {
            //Update the best tour
            best_sol = inst->best_sol;
            best_val = inst->best_val;

            if (inst->verbose > 3)
                printf("Best val: %lf\n", best_val);

            //Update the tour
            instance_to_tour(best_sol, inst, tour_inst, nnodes);

            prob_trials = 0;
        }

        //Reset the lower and upper bounds
        set_lb_and_ub(env, lp, inst, 0, 1);
    }

    if (wallEnd(inst->wall_start) < inst->timelimit)
        printf("TIME LIMIT EXPIRED.\n");

    if (prob < 0)
        printf("EXACT PROBLEM SOLVED.\n");

    strcpy(inst->solutionapproach, "hard_fixing");

    free(sorted_tour);
    free(fixed_edge);

    return 0;
}

void instance_to_tour(double *best_sol, instance *inst, tour *tour_inst, int nnodes)
{
    //Create a temporary structure and initialize it
    point *connections = (point *) malloc(nnodes * sizeof(point));
    for (int i = 0; i < nnodes; ++i)
    {
        connections[i].x = -1;
        connections[i].y = -1;
    }

    for (int i = 0; i < nnodes; ++i)
    {
        for (int j = i + 1; j < nnodes; ++j)
        {
            if (best_sol[xpos(i, j, inst)] > 0.5)
            {
                if (connections[i].x == -1)
                    connections[i].x = j;
                else
                    connections[i].y = j;


                if (connections[j].x == -1)
                    connections[j].x = i;
                else
                    connections[j].y = i;
            }
        }
    }

    for (int i = 0; i < nnodes; ++i)
    {
        set(tour_inst->tour_nodes, i, (int) connections[i].x, (int) connections[i].y);
    }

    free(connections);
}

void set_lb_and_ub(CPXENVptr env, CPXLPptr lp, instance *inst, double lb, double ub)
{
    char upper_bound = 'U';
    char lower_bound = 'L';

    //Set to zero all the upper bounds
    for (int i = 0; i < inst->ncols; ++i)
    {
        if (CPXchgbds(env, lp, 1, &i, &upper_bound, &ub)) print_error(" wrong change coefficient");
        if (CPXchgbds(env, lp, 1, &i, &lower_bound, &lb)) print_error(" wrong change coefficient");
    }
}